/*jshint esversion: 11 */

module.exports = {
   config: {
      version: '1.0',
      folders: {
         js: {
            build: 'assets/js',
         },
         styles: {
            build: 'assets/css',
         },
      },
      opts: {
         gcmq: false,
         siteData: 'all',
         markdown: true,
         blogs: true,
      },
      settings: {
         marked: {
            gfm: true,
            tables: true,
            breaks: false,
            pendantic: false,
            smartLists: true,
            smartypants: false,
         },
      },
   }
};
