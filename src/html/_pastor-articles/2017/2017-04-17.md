---
layout: "articles.njk"
title: "Resurrection Disciples Do You Love Me?"
date: "04-17-2017"
slug: "resurrection-disciples-do-you-love-me"
summary: "After Jesus is raised from the dead there is this wonderful scene on the beach in Galilee, where Jesus deals with his friend. They have breakfast together after a record breaking catch, and Jesus is alone with Peter (John 21). Jesus asks Peter, 'Do you love me?' The great power of this question is more obvious when we read it in Greek. The word for love Jesus uses is 'agape,' which means unconditional love. So, Jesus is asking Peter, 'Do you love me unconditionally?'"
---

Jesus has shown Peter the most amazing mountain top experience that anyone can imagine. Peter has witnessed Jesus transfigured, and has met Moses and Elijah in a vision (Matthew 17:1-13). He must be feeling pretty good about himself as they are walking down the mountain. At the bottom of the mountain his fellow disciples cannot cast out a demon, and there is nothing he can do to help. This was only the beginning of the failures that Peter would now face as Jesus starts walking to Jerusalem, and his death on the cross. He would be rebuked consistently for not understanding God's plan, fall asleep and run away when Jesus needed him, and deny he even knew Jesus during the sham trials leading to Jesus' death. Peter needs to be set free from the bondage that is destroying his life with shame and guilt before he can become a resurrection disciple.

After Jesus is raised from the dead there is this wonderful scene on the beach in Galilee, where Jesus deals with his friend. They have breakfast together after a record breaking catch, and Jesus is alone with Peter (John 21). Jesus asks Peter, &quot;Do you love me?&quot; The great power of this question is more obvious when we read it in Greek. The word for love Jesus uses is &quot;agape,&quot; which means unconditional love. So, Jesus is asking Peter, &quot;Do you love me unconditionally?&quot;

Peter's answer is profoundly sad, for he answers Jesus with the Greek word &quot;phileo&quot; which means to love like a brother. Brotherly love is very special. It is the kind of love we share with our closest friends, but it is not agape, unconditional love. Peter answers, &quot;Yes Lord, I love you like a brother.&quot; Can you hear Peter remembering every time he denied Jesus? Do you think Peter was thinking, &quot;I thought I loved you unconditionally, until I denied you three times???&quot;

Then we have the power of forgiveness and the love of Jesus to transform our lives show itself in all love's beauty. Jesus tells Peter, &quot;Then feed my lambs.&quot; Peter is forgiven. Jesus needs him to do His work. Peter's sins do not negate the Lord's power to love, forgive, and call us to love and serve others.

Jesus asks Peter two more times, &quot;Do you love me unconditionally?&quot; Peter answers two more times, &quot;I love you like a brother.&quot; Twice more Jesus affirms that Peter is still a disciple with a great job to do to feed and care for all the lambs Jesus will send him to minister too.

There are three things that people who want to be resurrection disciples can learn from this amazing resurrection encounter. Peter would learn to trust his love for Jesus Christ unconditionally, even to death. Resurrection disciples never stop learning more about love. God is love (I John 4:8). Therefore, we can always go deeper, learn more, and be astounded by the power of love in our lives. Think about all the lessons you have learned about love, songs you have sung, poems you have read, and stories about the power of love you have heard. Never stop learning about, and applying a deeper love to your life.

We do not have to have it all together to do the Lord's work. We can certainly thank the Lord for that since just when we think we have it all together we find ourselves in places that Peter found himself. We can trust God loves us too much to leave us where He finds us, and so from God's perspective we never get it all together. There is always more He can teach us, stretch us, and grow us. Will you let Him?

Finally, resurrection disciples let the power of forgiveness flow in our lives to bring healing, wholeness, and new life to ourselves and those around us. If Jesus could look out upon the crowd at Golgotha and say, &quot;Father, forgive them for they don't know what they are doing&quot; (Luke 23:34), is there any sin we can't forgive in our own lives?

This concludes this series on resurrection disciples. I invite you to go back and write down all the questions I posed throughout the series and answer them for yourself. Decide to make any necessary changes, and ask the Lord to help you become a resurrection disciple. It won't be easy, but it will fill your life with meaning and purpose.
