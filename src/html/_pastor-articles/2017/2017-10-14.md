---
layout: "articles.njk"
title: "Dealing With Our Own Mortality"
date: "10-14-2017"
slug: "dealing-with-our-own-mortality"
summary: "I have heard it said that until you learn to live with your own mortality you can't really begin living. Fear of death is a huge giant in our culture."
---

We gathered around this great woman of faith.  Her son had brought her to his home so she could live her last days surrounded by family.  Her favorite gospel hymns were playing in the background.  All of her favorite people were around her bed sharing words of love, memories of good times, and holding her hands.  Someone would regularly cool down the washcloth on her forehead.  I was honored to be her pastor, and share this precious moment in her life as we surrounded her with love so she could pass peacefully from this life into the next.  When her shallow breathing stopped completely there were some tears, and then we shared a prayer thanking God for His promises that at death we have more than just memories, we knew this woman of faith was getting her room in the mansion with many rooms Jesus told His disciples about (John 14:2).

I have heard it said that until you learn to live with your own mortality you can't really begin living.  I think that is true.  Fear of death is a huge giant in our culture.  Too many hope they can whisk a dying loved one away to nursing homes, hospitals, or anywhere else they can let someone die without dealing with it.  Yet we who are in Christ have nothing to fear when it comes to death.  Let me break that thought down for you.

There are a number of reasons people fear death.  We fear the death of others because we do not want to lose them.  The loneliness and sadness are emotions we don't like to experience, and sometimes it takes a long time to learn to live with those emotions.  This is often made worse when the death occurs outside the natural order.  When a parent dies we miss someone dear to us, who knows us in a special way.  We miss them, and we are sad, but our parents are supposed to die before us.  

When a spouse dies this is not quite the natural order, because we usually can't imagine what life would be like if our spouse dies before us.  I have always been pretty comfortable with my own mortality, but the thought of losing my wife is always scary.   The struggles are many more than when a parent dies, especially if your spouse was your best friend.  The loss of your closest companion takes time to heal.

The loss of a child is completely unnatural and probably the hardest loss of all.  I have often heard parents who have lost a child say, &quot;I don't ever get over it; I just learn to live with it.&quot;

I think the most important thing to do when someone you love dies is to trust them to God.  Our Christian faith assures us we need not fear the death of our loved ones who have faith in the Lord.  Some of the wonderful promises of scripture include the promise that God is with us always, and walks with us down the valley of the shadow of death (Psalm 23).  The promise of the sure hope of the resurrection found in I Corinthians 15 is an amazing promise.  God reveals here that, at death, we will be given a new body fitted for the resurrection life.  John 14:1-7 uses familiar imagery of a great mansion, with so many rooms that there is room for everyone who finds faith in Jesus Christ.  

Are you going through a time of grief now?  Take time to read the scriptures noted above over and over again.  Can you think of other scriptures that give you hope and healing in the face of death?  I encourage you to memorize them so you can call them to your mind whenever you are struggling.  Have you prayed regularly for God to heal your grief?  When was the last time you lifted up such a prayer?  

Our faith gives us the sure confidence that death may be a fearsome giant, but through the power of God this giant can be slayed, and life has the victory, as we celebrate the life God has given us now and for eternity.
