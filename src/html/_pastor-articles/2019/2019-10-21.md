---
layout: "articles.njk"
title: "Sexual Lust and Purity"
date: "10-21-2019"
slug: "sexual-lust-and-purity"
summary: "Many men and women have lost much because they thought they could manage their sexual lust, and then they find their actions cost them their marriage, their family, their position in the community, and at work."
---

God has given us the gift of sexual intimacy as a wonderful gift for men and women in the holy bond of marriage.  Great marriages always include active sexual and emotional lives that fulfill both the husband and the wife.  In the Old Testament Book, &ldquo;The Song of Solomon,&rdquo; Solomon and the Shulammite woman have a passionate and erotic monogamous relationship.  This is God's desire for couples in marriage.

One of the most tragic stories of the Bible is David's affair with Bathsheba, the betrayal of her husband Uriah, the many ways it weakened the nation of Israel, and the collapse of David's family (see II Samuel 11-12 for all the gory details).  Many men and women have lost much because they thought they could manage their sexual lust, and then they find their actions cost them their marriage, their family, their position in the community, and at work.

When facing the sin of lust, the first thing you need to remember is when you are tempted you must flee.  This is what Joseph did when Potiphar's wife tried to seduce him (Genesis 39).  He didn't try to reason with her or some other clever move.  He ran away as fast as he could.  When you are at your computer and you are tempted to view web sites that are impure you will eventually view them, unless you just leave and do something else.  Get away from your computer.  In the workplace, if someone is flirting with you, don't think you can handle it, and enjoy the attention.  Do not let yourself be alone with that person.  Guard your heart with a mighty shield of protection.

Second, it is important to know that for both men and women there are gateways that lead to sexual impurity.  Everyone knows that pornography can be very tempting for men.  The internet has opened the door to easy and private access to pornography of every kind.  That is why it is important to have your computer in an open place for all to see, with filters to control use of the computer.  Further, giving someone access to your history of searches will bring accountability, which is crucial to sexual purity.

Not as well known is that a common gateway for women is romance novels.  These stories often lead to discontentedness with their husband, and a willingness to have &ldquo;friendly&rdquo; chats with other men or old boyfriends.  This is a land mine waiting to destroy the relationships that are important to you.

In addition to knowing the gateways we open to enjoy lust we must also know and avoid tempting situations we &ldquo;accidentally&rdquo; put ourselves in. The Bible says King David should have been with his men in battle, but instead he was on the rooftop of his home open to whatever his wandering eyes might see.  He was vulnerable because he put himself in a tempting situation and there was no one around to hold him accountable (II Samuel 11:1-4).

If you must travel during your work don't go to places that compromise your integrity.  Have the hotel turn off access to adult television before you get to your room.  Have someone you trust hold you accountable for your actions. Lack of accountability opens the door for us to do things we would never do if we thought our family or friends would find out.

Finally, you must decide in advance what you will do in tempting situations.  Take a look at your life.  What are the circumstances that can come up that can cause you to be tempted?  Are you vulnerable to flirting?  Are you alone in far away towns with no one to hold you accountable?  Does your job involve visitation with individuals of the opposite sex when you are alone?  How do you protect yourself from untrue accusations as well as impure temptations?  Some other questions to consider include what are the gateways that tempt you into deeper sexual impurity?  What are your greatest temptations to impurity?  Do you have access to the internet without accountability?  How can you change this situation?

The passion Solomon and the Shulammite woman share is a part of God's desire for every married couple as the two become one (Genesis 2:24, Mark 10:8).  My hope is you will avoid impurity, and enjoy the wonderful gift of sexual intimacy as God has designed it to be enjoyed.