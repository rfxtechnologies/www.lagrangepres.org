---
layout: "articles.njk"
title: "Who Have Been/Are Your Spiritual Mentors?"
date: "05-02-2022"
slug: "who-have-been-your-spiritual-mentors"
summary: "Most people need someone to take an interest in them, invest in them, and help them to grow as a disciple."
---

Who Have Been/Are Your Spiritual Mentors?

Most people have a mentor or mentors they think about who helped them learn more about their career than anyone else.  Perhaps a wise employer who taught you habits and skills that would help you the rest of your life in your chosen career.  Have you ever thought about who your spiritual mentors are?  Dr. James Dobson his book Parenting Isn’t for Cowards, writes, &ldquo;Attach a boy to a good man, and he seldom goes wrong.&rdquo; Dr. Terry Wise defines mentoring as: &ldquo;A relational experience in which one person empowers another by sharing God given resources.&rdquo;

I have been blessed with some great mentors in my life.  Growing up in the Presbyterian Church Pastor Bing Summerell provided a great ministry role model and was always there for me during his life.  He helped me through many questions of faith on my road to becoming a minister.  Who are the people who have impacted your growth as a disciple?  You may or may not think of them as building a relationship with you, but their integrity and wisdom impacted the person you became.

It is rare that people seek out spiritual mentors in a formal way, but this is to our detriment. Usually, God moves people in and out of our lives who impact us, and they don’t know they have impacted us, nor do we often tell them thanks for investing in our lives.  In 1 Thessalonians 2:8, Paul shares with this congregation how he thinks of his relationship with them as a mentoring relationship, hoping they will grow spiritually.  He writes, &ldquo;Having so fond an affection for you, we were well-pleased to impart to you not only the gospel of God but also our own lives, because you had become very dear to us.&rdquo;

If you have never looked for a spiritual mentor I want to invite you to do so, and formalize the relationship by meeting with that person on a regular basis.  If you have some years of experience as a disciple, and you see a person who is young in their faith journey I want to challenge you to look for opportunities to invest in that person, and mentor them in the faith.

Have you ever thought of Jesus as a mentor?  Thinking this way could change the way you approach your journey of discipleship.  The one essential condition for being mentored is that you must spend time with your mentor.  You can’t do discipleship at arm's length or through distance learning. Too many Christians spend their time trying to do the minimum needed to assure themselves of salvation.  If we want Jesus to mentor us like He did His disciples then we have to study His word, have plenty of time in prayer, worship God regularly, tithe or give sacrificially, serve others, spread the Good News of the Gospel, and care about the people in our spiritual family like they are family.  These were the things He taught His disciples to do.

W. W. Wiersbe writes in The Bible Exposition Commentary &ldquo;...In most churches, the congregation pays the pastor to preach, win the lost, and build up the saved, while the church members function as cheerleaders (if they are enthusiastic) or spectators. The 'converts' are won, baptized, and given the right hand of fellowship, then they join the other spectators. How much faster our churches would grow, and how much stronger and happier our church members would be, if each one were discipling another believer.&rdquo;

Dan Riemenschneider wrote in The Missionary Church, Inc., &ldquo;Discipleship is like the generation gap between parents and children: We often tell kids to grow up. They can’t grow up. They need mentoring. They can't grow up because they've never been there before. We as parents, have been there. The goal is not for them to grow up but for us to become like them and show them how. That's what Jesus did. He became like them to show them how.&rdquo;

Who have been your spiritual mentors during your life?  Share with someone the lessons your mentor(s) have taught you through the years.  If possible, reach out and say, &ldquo;Thanks,&rdquo; to them for the role they played in your life.  Who is your spiritual mentor today?  Would you grow more in your faith if you had one?  Who are you trying to mentor?  Who do you know that could benefit from your spiritual wisdom and encouragement?  Don’t sell yourself short!  Most people need someone to take an interest in them, invest in them, and help them to grow as a disciple.
