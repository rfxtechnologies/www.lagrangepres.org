---
layout: "articles.njk"
title: "How Often Do You See God’s Miracles?"
date: "05-30-2022"
slug: "gods-miracles"
summary: "Many will remember the show, 'Candid Camera,' where a hidden camera catches people in the act of being people.  Sometimes we can learn something significant about human nature, like how people act when they witness a miracle."
---

Many will remember the show, &ldquo;Candid Camera,&rdquo; where a hidden camera catches people in the act of being people.  Sometimes we can learn something significant about human nature, like how people act when they witness a miracle.

One miracle involved rigging a bird cage with a swinging perch, plants moving due to a flapping bird's wings, and a bird whistle. Everything was normal, but there was no bird.  People were maneuvered to look in the cage, and their reaction was filmed.  Down to the last person, no one noticed anything wrong.  When asked to describe the bird, many gave vague descriptions.  Some were very specific, including beak color, tail length, etc.  They saw what they expected to see, a cage with a bird in it.

This example, I think, reveals some truth about the way we respond to miracles in our lives.  Like the bird in the cage, which is NOT in the cage, we see what we want to see.  And if we want to see a world without miracles, that is what we see.

Many of us logical, well-educated, sophisticated people of the modern world are inclined to move rather quickly through the miracle stories of Jesus.  We try to explain what happens in modern scientific terms, and surmise that even though the people did not understand epilepsy, hernias, and psychosomatic illnesses, Jesus, being the Son of God, must have.  Perhaps he was a fine doctor ahead of his time.

I believe that we are surrounded by miracles.  If we doubt their presence in our lives, it is because we'd rather see a world without miracles and pretend they aren't there.  The reason Jesus seemed to be associated with miracles, is that he not only had the ability to work miracles, he could also lift up the miraculous side of life so the people had to see the miracles and accept their reality.

Miracles have always been a key part of the Christian faith, and better than glossing over them in the Bible, I think we should celebrate the wonder of life, creation, and God's mysterious ways, as those who ran after Jesus did.

Driving around on the roads sometimes resembles a demolition derby, but, to date, each of us has survived.  Is that a miracle?  How many times in our day-to-day activities have we had &ldquo;close calls,&rdquo; but no injuries or fatalities? Good fortune comes our way at the perfect time so that we are spared in accidents that could have been so much worse.  These things happen so regularly, people speak of having a guardian angel.

I decided to test this theory that there are a lot more miracles going on around me than I think by starting a miracle journal.  Everyday I reflected on the day and whether I had seen any miracles, that is events that occurred that had no easy explanation or a series of circumstances fell perfectly into place that it seemed God’s guiding hand was at work.  At first I noticed that I had more miracles happening around me than I ever realized.  I would have guessed that before starting the journal I would experience a miracle a week.  Then as my ability to see God at work in my life improved it seemed like God was working a miracle a day or more.  Then I began to see that God was actually working in and through every moment of my life and I had to start recording only the most profound miracles where I saw the hand of God each day.

This gave a whole new meaning to the what we read in John 21:25, “Jesus did many other things as well. If every one of them were written down, I suppose that even the whole world would not have room for the books that would be written.”

Are we really surrounded by miracles?  Is the body’s tremendous capacity to heal itself a miracle?  How many times do doctors treat problems they don’t understand, yet they know the body will heal?  How many times have doctors given up, and people experience a healing miracle?  Is it a miracle how few accidents, both in our cars and around work, home, and recreation that we experience?  Have you ever had a sense there was a superintending power or guardian angel protecting you?  Have you ever met anyone with an addiction who could not break free, but then faith in Jesus Christ changed them and they found freedom?

Are we really surrounded by miracles?  Perhaps it depends on what we are looking for!  I recommend you keep a Miracle Journal for a month and see what happens.
