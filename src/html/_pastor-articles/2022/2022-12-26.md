---
layout: "articles.njk"
title: "Timeless Stories that Exalt the Divine Likeness of Humanity"
date: "12-26-2022"
slug: "timeless-stories-divine-likeness"
summary: "We need to remember who we are. The Bible teaches us that we are made in the image of God."
---
What makes a story timeless? The stories that dig deep into who you and I are, and how we deal with life’s greatest struggles
and mysteries give us a glimpse of the complexity of the human soul. The stories of King Arthur’s Camelot, and the Knights of
the Round table fit this description. We get a glimpse of the struggle of love and faithfulness in the story of the adulterous
relationship between Queen Guinevere and Arthur’s most trusted knight, Sir Lancelot. The scheming Mordred catches them in a
clandestine encounter. Lancelot is able to escape, but Guinevere is not so fortunate. When she faces her trial, the jury finds
her guilty and sentences her to burn at the stake.

As the day of execution nears, people come from miles around with one question in their minds: Would the king let her die?
Mordred gleefully captures the complexity of Arthur’s predicament, “Arthur! What a magnificent dilemma!  Let her die, your life
is over; Let her live, your life’s a fraud.”

Tragically but resolutely, Arthur decides, “Treason has been committed! The jury has ruled! Let justice be done!” High from the
castle window stands Arthur, as Guinevere enters the courtyard. She walks to her unlit stake, where the executioner stands
waiting for Arthur to give the signal. He cannot do it.

Seeing Arthur crumble, Mordred relishes the moment, “Well, you’re human after all, aren’t you, Arthur? Human and helpless.”
Tragically, Arthur realizes the truth of Mordred’s remark. Being only human, he is indeed helpless. But where this story ends,
the greatest story ever told just begins.

It is another execution scene. It is another time, another place, and another king. The setting: A world lies estranged from the
God who loves it. Like Guinevere, an unfaithful humanity stands guilty and in bondage, awaiting judgment’s torch. Could God turn
His head from the righteous demands of the law and simply excuse the world’s sin? What God does is the greatest story ever told.
He leaves His great throne and comes into the world as a little child. He becomes the perfect man, the perfect sacrifice, willing
to die for the sins of the world. When Jesus asks if He can escape the cup of death His Heavenly Father answers, “No.” The reason
is God has a perfect plan to save humanity from our sins.

To better understand this, we need to remember who we are. The Bible teaches us that we are made in the image of God, with our soul
and our bodies perfectly combined to make us human with God’s spirit dwelling within us. This is in direct contradiction to the
natural and material view the world teaches us, which is that we have evolved out of the muck and the mire of primordial goo.

The Bible teaches that we have a moral problem because we were created perfect, but we have a fallen nature due to our rebellion
against God, and we are desperately wicked (e.g. Romans 8:13). The material world teaches that we have no such moral dilemma, but
that we are basically good. If we follow our hearts and desires, we will achieve our highest goals of self-actualization in life. I
wonder if the Sir Lancelot’s and Guinevere’s of the world agree with that?

The Bible teaches that our greatest need is divine grace and redemption, and we desperately need a Savior (e.g. Galatians 5:15-17).
The material world teaches that we must save ourselves through self-oriented pursuits. Evil will go away if we can properly reform
the unfair social institutions (e.g. family, marriage, church, state, and education). The way the Bible looks at the world and the
way the natural and material world looks at the world are completely different.

Here are some questions I hope you will ponder and ask someone else what they think. Which one is truth, and which one is lies? How
does the way we understand the nature of humanity affect the way we live, think, vote, and make decisions? Which view dominates in
our schools, community, and courts? Are we pleased with the results? Which view dominates in our homes? Which are we teaching to our
children? The birth of the Christ child is God’s way of saying it is a matter of life and death which view you choose. Do you still
struggle with the truth of scripture? What do you need to have happen to decide, once and for all, that the Bible is the final
authority for faith and practice in your life? I hope you will say a prayer thanking God for His holy word, the Bible, and what it
means to you today. (To find out more about Al Earley or read previous articles see, www.lagrangepres.org).