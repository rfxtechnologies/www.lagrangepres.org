---
layout: "articles.njk"
title: "New Year Thankfulness In Advance"
date: "01-02-2023"
slug: "new-year-thankfulness"
summary: "We are to learn to be thankful for those things we think are bad as well as the good."
---
As we begin a new year I invite you to consider this question, “What do we know God is going to do for us next year?”
You can make an amazing list of things you will be thankful for next year around the most important areas of faith,
family, health, and provision. We know because God has done these things for us all our lives. Maybe a year passes
here or there when blessings don’t shower down in one of these four areas, but the other three often make up for it.

But what if next year becomes the year from hell. We know terrible things happen. What if we have a terrorist attack
in our community, the financial markets crash, we lose a parent, spouse, or child, or our health is stricken, and we
find out we may die. I have good news. God has us covered. We live in a fallen world. There is sin, death, and evil all
around us, and faith doesn’t change that. But if you have confessed Jesus Christ as your Lord and Savior then you are
to believe that Jesus has set you free from sin, death and, evil.

There is a challenging verse in I Thessalonians 5:18 where God wants to teach us to be thankful in advance. We read,
“Be thankful in all things, for this is the will of God in Christ Jesus for you.” We are to learn to be thankful for
those things we think are bad as well as the good. God is working through all things for good in our lives when we love
Him (Romans 8:28). So, He is going to use the bad things as well as the good to bless us and our lives.

Maybe you are not quite sure about thanking God in advance. Let’s take the big three curses one at a time. The first is
we are sinful, we are separated from God, and we cannot save ourselves. Our sins and the sins of others against us are
covered by the blood of the lamb, Jesus Christ. We have Jesus’ example of forgiveness from the cross to assure us that there
is great power in forgiveness to heal our broken relationships. That as Christians we are to forgive offenses, grudges, and
sins against us to be free of anger and bitterness. Our faith gives us courage to ask for forgiveness and be forgiving.
If you go to God in prayer, feel led by God to act, and trust God to work miracles then you can thank God in advance for the
healing He is going to bring, even if the situation turns bad or looks bleak. God has the problem of sin completely covered
this next year.

What about death? The promises are so many to help us be free of fearing of death. In I Corinthians 15 we read about the sure
hope of the resurrection. God promises to give us a spiritual body for eternity just like he did for Jesus. Heaven is a perfect
place with no death, crying, pain, or illness. You think, “I’m not afraid of my death, but the death of my spouse/child is
terrifying.” God promises to be with us always, the healing balm for all our pain. If we can trust God with everything in our
lives, then we need not fear death.

Then there is evil. In Ephesians 6:10-18 God assures us our battle is not against flesh and blood but against the dark forces of
this present age and He equips us with His full armor, so we can fight every battle against evil and win. James 4:7 and dozens of
other texts assure us that at the name of Jesus all evil must flee. Jesus gives us the power to bind up evil in Matthew 18:18. In
I John 4:4 we read, “Greater is He (Jesus) than he (satan) that is in the world.”

Jesus shows us how to thank God in advance before He raises Lazarus from death. Jesus prays, “Father, I thank you that you have
heard me. I knew that you always hear me, but I said this for the benefit of the people standing here, that they may believe that
you sent me” (John 11:41-42).

Will you take fifteen minutes to make a list of things to thank God for in advance of this new year to come? Is there anything
bothering you right now that grows out of sin, death, or evil? Give it to God, dream about how God might bring it to a miraculous
conclusion, and thank God for that in advance as we begin a new year. (To find out more about Al Earley or read previous articles
see, www.lagrangepres.org).