---
layout: "articles.njk"
title: "The Beautiful Brevity of Cardboard Confessions"
date: "04-17-2023"
slug: "the-beautiful-brevity-of-cardboard-confessions"
summary: "At our Easter service everyone that wrote down their 'Cardboard Confession' talked about what a beautiful experience it was to write down their struggle and Christ’s victory as briefly as possible"
---
One Easter morning, a woman was on her way to church when her car broke down. She ordered an Uber to pick her up so she wouldn’t be late to worship. The car arrived, and she quickly jumped in the back. Halfway through the ride, she asked the driver a question, but the driver didn’t respond. So, she leaned forward and tapped the driver on the arm. The driver let out a loud scream, swerved into the other lane, almost hit another car, slammed on the brakes, and skidded over to the shoulder.

The woman and driver sat in silence for a minute from the shock of what just happened. Finally, she said apologetically, “Wow, I’m so sorry. I had no idea that tapping your shoulder would alarm you like that.”

“No, you really didn’t do anything wrong. It’s just that it’s my first day driving an Uber. You see, for the past 25 years, I’ve been driving a hearse.”

I think the greatest proof of the resurrection is the new life it brings to people’s lives. New life is not normal. Death, decay, and destruction are normal. People’s lives getting smashed by tragedy, shame, and guilt are normal. And then people carry those burdens around like a ball and chain for the rest of their lives. Faith in Jesus Christ can change all of that.

Several years ago, I asked my congregation to help me preach the Easter sermon. I had a big stack of blank cardboard sheets and invited the congregation to write a simple summary of some life struggle they once had. To help them think about a life struggle, I asked some questions. Questions like, “When did you feel most like a prodigal son or daughter? When have you felt furthest away from your heavenly Father?”

Then, I told them to turn the card over and write down how God resolved their life struggle in a simple statement. Again, I asked questions like, “How has the life, death, and resurrection of Jesus Christ transformed you from a prodigal to a son/daughter of the King?  How did God use Your faith in Jesus Christ to turn your life around?”

After that, people were invited to come to the front of the sanctuary, show their “Cardboard Confession,” count to five, turn their sign over, count again, and smile! What would your cardboard confession be?

In John 21:15-19 we have the beautiful scene where Jesus reinstates Peter on the beach after the resurrection. Jesus asks Peter, “Do you love me unconditionally (agape in Greek)?”

Peter has the most honest moment of his life and says, “I love You like a brother (phileo in Greek).” Peter says the truth. How could he tell Jesus he loves Him unconditionally? Before Jesus’ death Peter denied the son of God three times after being warned he would do so.  He had promised to follow Jesus anywhere, and he failed. He fell asleep when he should have been praying. What would Peter’s Cardboard confession look like? One side might read, “On the most important night of my life I slept, failed, and fled.”

The power of forgiveness in the name of Jesus Christ changes everything. What might Peter write on the other side of his Cardboard Confession? I think it might say, “Jesus’ forgiveness set me free to serve Him!”

At our Easter service everyone that wrote down their “Cardboard Confession” talked about what a beautiful experience it was to write down their struggle and Christ’s victory as briefly as possible. I invite you to spend a moment to do the same. You can get out a 3X5 card and write down your life struggle on one side. If you have trouble thinking of what to write down, think of it as Sinner’s Block. Turn to Ephesians 4:25-32 where Paul lists many sins to avoid in life. That should help remind you of a life struggle that sin has brought into your life.

Then turn the card over and write down how your faith in Jesus Christ resolved your struggle. It is a powerful experience to summarize how bad you need the cross, and how Christ’s death and resurrection sets you free. People often wonder how their lives would be different if they didn’t have Jesus. Making a simple “Cardboard Confession” can help you know the answer to this question.

When we take time to celebrate how our faith has changed our life, like thinking about this simple exercise, we realize that we can celebrate Easter, new life in Christ through His resurrection every day of our lives. Look again at your “Cardboard Confession” and decide to live each day as if it is true!!! (To find out more about Al Earley or read previous articles see, www.lagrangepres.org).
