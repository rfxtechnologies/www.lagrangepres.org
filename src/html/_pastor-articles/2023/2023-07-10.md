---
layout: "articles.njk"
title: "The Meaning of 666"
date: "07-10-2023"
slug: "the-meaning-of-666"
summary: "Revelation tells us clearly that the number 666 will be a number used by the antichrist for commerce, and to weed out believers for death."
---
The Book of Revelation is full of apocalyptic imagery and symbols that are supposed to help us know if we are in the end times.  Perhaps the best known and controversial part is Revelation 13:18, “This calls for wisdom. Let the person who has insight calculate the number of the beast, for it is the number of a man.  That number is 666.”

Everyone knows that the number of the beast is 666.  Did you know that the number of the millibeast is 0.666.  The zip code of the beast is 00666.  The retail price of the beast is $665.95 (have you had enough yet?).  The oven temperature for cooking roast "beast" is 666 F.  And finally, the license plate number of the beast is IAM 666.    
     
What do we make of the number 666?  I make nothing of it.  It has no more meaning than the number 13.  I don’t want you to be a Hexakosioihexekontahexaphobiacs, which means people who fear the number 666.  Revelation tells us clearly that the number 666 will be a number used by the antichrist for commerce, and to weed out believers for death.  The specifics are not clear, and since none of this is happening now, the details are really conjecturing.  
     
Perhaps you are wondering how the antichrist fits into this picture?  It is generally assumed that the antichrist, the man of lawlessness, and the beast in Revelation 13 all refer to the same end time enemy of God.  Further, in every age people have believed they knew who the antichrist was.  Some candidates through the centuries include the pope in 1000 A.D, Hitler, Stalin, Khomeini, Bin Laden, and Presidents Bush, Obama, and Trump depending on your political affiliation.  
     
So, is the antichrist alive today?  I believe one of the reasons God gave us Revelation is that it will begin to make sense and be a guide to what is going on in the end times.  I do believe that many of the pieces are in place so that if God decides it is time to reestablish the Kingdom of Heaven on earth it could be in our times.  But there are also things in Revelation that are not in place, so we continue to wait for God’s perfect timing.  
     
Another reason God gave us Revelation is to give the faithful confidence and courage in the face of religious persecution.  Throughout the book, one of the overwhelming messages is that God wins, so keep the faith no matter what.  That is what 5898 people needed to believe in 2022.  That’s how many people died according to the advocacy group Open Doors.  That was up from 4761 who were killed in 2021.  They also calculated that 360 million Christians were persecuted for their faith in 2022, up by 20 million from 2021.  
     
It should not be surprising that more and more Americans are now experiencing persecution for their faith as well.  Swimmer Riley Gaines was invited to be a guest speaker at the University of San Francisco.  When she arrived to give her speech, she was threatened with death, hit, and locked in a room for her safety for three hours.  She was fighting for the rights of biological women to compete against other women, not biological men.  A fight she fights through her faith in Jesus Christ.  
     
What if you find yourself in the middle of a controversy where what you believe is being challenged?  Is there a Godly way to stand up for your faith?  Paul offers excellent guidance in II Timothy 2:24-25, “And the Lord’s servant must not be quarrelsome but must be kind to everyone, able to teach, not resentful.  Opponents must be gently instructed, in the hope that God will grant them repentance leading them to a knowledge of the truth...”
     
If I am asked to help someone I need to earn the right to be heard.  I will not earn that right by resorting to social media and blasting incendiary bombs on unsuspecting people.  I will not be quarrelsome.  Respect and gentleness are the words that describe the Christian that faithfully defends the faith if they believe in the power of God to accomplish His purposes over time.  When we remember that God is not limited by our time limitations to accomplish His purposes we can trust God to be victorious through our faithfulness.
     
Do you believe your faith could/would survive persecution?  Would you speak up if what you believed was challenged, or would you remain quiet?  What do you need to do now to build the kind of courageous faith necessary to endure hard times?  As we see our culture becoming more and more antagonistic toward our faith the challenge may come when we least expect it (To find out more about Al Earley or read previous articles see, www.lagrangepres.org).
