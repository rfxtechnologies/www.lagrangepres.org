---
layout: "articles.njk"
title: "Does God Ever Give Us More than We Can Handle?"
date: "07-31-2023"
slug: "does-god-ever-give-us-more-than-we-can-handle"
summary: "It is the love of God and trust in God’s will that allows God to bring all things to good in our lives."
---
One of the powerful scriptures that people like to quote is from Romans 8:28.  It reads, “…We know that in all things God works for the good.”  Like the Bible verse that says, “God never gives you more than you can handle” these verses comfort people in times of challenge.  The problem is one is incomplete and the other is not in the Bible, and neither, as they are printed, are true.

First, the verse from Romans actually reads, “…We know that in all things God works for the good of those who love him, who have been called according to his purpose.”  The second half makes the first half true.  I know plenty of people who have been crushed by life’s trials never to recover.  It is the love of God and trust in God’s will that allows God to bring all things to good in our lives.
     
Second, nowhere in the Bible does it say, “God will never give us more than we can handle.” God gives us more than we can handle whenever He thinks we are ready to come closer to Him, and to trust Him with our lives at a deeper level.  When those tests come, we find that we come to the end of our personal strength, and we have two choices, trust God more, or turn away and try to go through the trial alone.  The second choice will almost always end badly.  The first choice almost always ends with the most positive life changing experiences that we have ever had.
     
This tragic story turned into victory wonderfully illustrates how loving God can bring good out of evil.  When Lianna was twelve years-old she was brutally raped by two men and left for dead.  As she was miraculously recovering she found out she was pregnant.  Her doctor suggested she solve the problem with an abortion.  He explained how it was the right thing to do for her so she wouldn't be haunted by a constant reminder of the attack.  As she thought about her choices, she asked her doctor, “Would an abortion ease her pain and help her forget about her rape?”
     
Her doctor said, “No.”
     
Lianna knew that she would keep the baby. She reasoned, “If abortion wasn't going to heal anything, I didn’t see the point.  I just knew that I had somebody inside my body. I never thought about who her biological father was. She was my kid. She was inside of me. Just knowing that she needed me, and I needed her...it made me want to work, to get a job [to support her].”
     
Healing after the rape proved traumatic, and she often thought about suicide, but the thought of killing her baby always stopped her.  She knew God was with her because her decision not to have an abortion turned out to be what saved her life.   Lianna said, “In my situation, two lives were saved. I saved my daughter’s life, but she saved my life.”  As the years passed she found that her daughter gave her life new meaning. She soon found each precious smile from her baby girl replacing her pain with something much stronger – love. And her daughter seemed to somehow be aware of her purpose as well.  When Lianna's daughter was just 4 years old, she turned to her and said, “Mommy, thanks for giving me life.” This tiny gift from God provided Lianna with all of the healing she needed.
     
Lianna's daughter is now a young adult, and the two have an incredible bond. One that was worth all the pain and suffering as far as Lianna is concerned.  "Even though [the rape] was a very hard moment, if I had to go through that [again] just to know and to love my daughter, I would go through that again."  Today, Lianna travels the world as a pro-life speaker. She's also started her own organization, Loving Life, whose mission is to promote the value of life, as well as help other victims of violence and abuse.  (edited from a story from www.godupdates.com).
     
We live in a fallen world where sin, evil, and death are very real.  God can bring good whenever we experience how fallen the world is if you love Him and respond to His call for our life.  When has God put more on you than you can handle on your own?  Did you run into God’s arms to find strength and new life, or did you run away from God and try to solve our problems on our own?  Did you trust God to get you through?  Did you love God during the struggle and find the good God intends for your life?  (To find out more about Al Earley or read previous articles see, www.lagrangepres.org).