---
layout: "articles.njk"
title: "What is the Statistical Probability that Jesus Is Who He Says He Is?"
date: "12-11-2023"
slug: "what-is-the-statistical-probability-that-jesus-is-who-he-says-he-is"
summary: "If you have any doubts this Christmas that Jesus is who He said He is, simply consider what the God inspired writers of the Old Testament said would happen."
---
There are many scriptures in the Old Testament that Christians claim Jesus fulfilled.  I have seen numbers from 100 to 436 different scripture texts written hundreds, even thousands of years before Jesus’ birth, that tell of a messiah God will send in the future who will redeem the world.  I have listed below just eight of the most popular and well known of those scripture texts, with the New Testament proof of their being fulfilled.

1. Jeremiah 23:5, "The days are coming," declares the LORD, "when I will raise up to David a righteous Branch, a King who will reign wisely and do what is just and right in the land.  See Matthew 1 & Luke 1 for the genealogy that shows Joseph was descended from David.

2. Micah 5:2, "But you, Bethlehem Ephrathah, though you are small among the clans of Judah, out of you will come for me one who will be ruler over Israel, whose origins are from of old, from ancient times."  See Matthew 2:1-6 and Luke 2:4 for confirmation Jesus was born in Bethlehem. 
     
3. Isaiah 7:14, Therefore the Lord himself will give you a sign: The virgin will be with child and will give birth to a son, and will call him Immanuel.  Matthew 1:18-23; Luke 1:27-31 confirm Mary was a virgin, impregnated by the Holy Spirit.
     
4. Malachi 3:1, "See, I will send my messenger, who will prepare the way before me. Then suddenly the Lord you are seeking will come to his temple; the messenger of the covenant, whom you desire, will come," says the LORD Almighty. John the Baptist was that messenger, confirmed in Matthew 3, Mark 1, and Luke.
     
5. Zechariah 9:9, Rejoice greatly, O Daughter of Zion! Shout, Daughter of Jerusalem! See, your king comes to you, righteous and having salvation, gentle and riding on a donkey, on a colt, the foal of a donkey.  All four Gospels confirm that Jesus rode into Jerusalem on the back of a donkey.  See Matthew 21, Mark 11, Luke 19:28 ff., and John 12:12 ff.
     
6. Zechariah 11:13, And the LORD said to me, "Throw it to the potter"-- the handsome price at which they priced me! So, I took the thirty pieces of silver and threw them into the house of the LORD to the potter.  Matthew 25:5-7 confirms Judas betrayed Jesus for thirty pieces of silver.
     
7. Psalm 22:18, “They divide my garments among them and cast lots for my clothing.”  Again, all four Gospels,  Matthew 27:35, Mark 15:24, Luke 23:34, John 19:24, tell of the centurions gambling for Jesus’ clothing at the cross.  
     
8. In the "Suffering Servant" passage of Isaiah 52-53 there is an astounding account of a man’s death that is described just like Jesus’ crucifixion, and it was written in 712 B.C.  We read in Isaiah 53:5, “But he was pierced for our transgressions, he was crushed for our iniquities; the punishment that brought us peace was upon him, and by his wounds we are healed.”
     
Professor Emeritus of Science at Westmont College, Peter Stoner, has calculated the probability of one man fulfilling these eight major prophecies concerning the Messiah. The estimates were worked out by twelve different classes representing some 600 university students. They concluded that the chances of one man fulfilling all these prophecies was 1 in 10 to the 17th power.  (Peter Stoner, Science Speaks, Chicago: Moody Press, 1969) 
     
Prof. Stoner suggests that “we take 10 to the 17th silver dollars and lay them on the face of Texas. They will cover all the state 2 feet deep. Now mark one of these silver dollars and stir the whole mass thoroughly.  Blindfold a man and tell him he can travel as far as he wishes to see if he can find that one marked silver dollar. What chance would he have of getting the right one?”
     
For the skeptic, there is likely no proof that I can offer to believe Jesus Christ is the Messiah.  But, if you have any doubts this Christmas that Jesus is who He said He is, simply consider what the God inspired writers of the Old Testament said would happen.  This Christmas I hope you will put your trust in Jesus without any reservations! 
     
What is the biggest doubt you have that Jesus is who He says He is?  Have you done the careful research to see why some don’t think this is a problem, and some do?  The mature Christian is not afraid to study carefully, and get the right answers.  The Bible will stand the test of time.  It always has and always will because it is the word of God!  (To find out more about Al Earley or read previous articles see, www.lagrangepres.org).
