---
layout: "articles.njk"
title: "God Is the Hound of Heaven, Chasing After Your Soul"
date: "12-30-2024"
slug: "god-is-the-hound-of-heaven-chasing-after-your-soul"
summary: "I could go on and on about amazing stories of coming to, and growing in faith. Maybe one of those stories is yours, as God changed your life through the extended hand of a woman in need. Was it the birth of your child? The explosion of a sunset? The impassioned sermon that moved all? The dull sermon that moved none but you?"
---

James Whittaker was a member of the handpicked crew that flew the B-I Flying Fortress, captained by Eddie Rickenbacker, during World War II. In October 1942, Rickenbacker and his crew were reported lost somewhere over the Pacific and out of radio range. The plane ran out of fuel and crashed into the ocean. The nine men spent the next month floating in three rafts. They survived the heat, the storms, the water and the sharks, some ten feet long, would ram their nine-foot boats. After only eight days, their rations were eaten or destroyed by the saltwater. It would take a miracle to survive.

James Whittaker was an unbeliever. The plane crash didn’t change his unbelief and the days facing death didn’t cause him to reconsider his destiny. In fact, Mrs. Whittaker said her husband grew irritated with John Bartek, a crew member who continually read his Bible privately and aloud. But, his protests didn’t stop Bartek from reading. Nor did Whittaker’s resistance stop the Word from penetrating his soul. For it was one morning, after a Bible reading, that the seagull landed on Captain Rickenbacker’s head when he had put his head back against the raft and pulled his hat over his eyes. He peered out from under his hat. Every eye was on him and he instinctively knew it was a seagull.
 
Rickenbacker caught it and the crew ate it. The bird’s intestines were used for bait to catch fish, and the crew survived to tell the story, the story of a stranded crew with no hope or help in sight. It was at that moment that Jim Whittaker became a believer.
 
Doesn’t God sound like the Hound of Heaven? Who would go to such extremes to save a soul? God is chasing after your heart. God wants your soul. God in Jesus Christ longs for you to know of His infinite and unfailing love for you. How do I know? Jesus tells us about it all the time in parables like The Lost Sheep (Matt 18:12-14) and The Prodigal Son (Luke 15:11-32). Jesus’ lifestyle was that of a man chasing after the souls of others, always preaching, always moving to new places, always healing and changing lives. Jesus said that he came to seek and to save the lost (Luke 19:10). Jesus’ death and resurrection are God’s perfect plan for the salvation of our souls so that we might know God is chasing after each one of us. If you have given your life to Him, He wants you to go deeper.
 
John Whittaker’s story has been repeated hundreds of millions of times over history because God is chasing after every one of us, trying to get us to believe in Jesus as Lord and Savior. John Wesley, founder of the Methodist Church, went unwillingly to a reading of Martin Luther’s Commentary on Romans. Now that sounds like fun, doesn’t it? He wrote, “About a quarter before nine, I felt my heart strangely warmed.”
 
Great Catholic theologian, St. Augustine, was trying to decide between following a tempting mistress or the Spirit of God. Some children were playing a game, and were hollering, “Pick it up. Pick it up.” He picked up the Bible beside him and read, “Let us live in the right way, like people who belong to the day. We should not have wild parties or get drunk. There should be no sexual sins of any kind, no fighting or jealousy. But clothe yourselves with the Lord Jesus Christ and forget about satisfying your sinful self” (Romans 13: 13-14).
 
I could go on and on about amazing stories of coming to, and growing in faith. Maybe one of those stories is yours, as God changed your life through the extended hand of a woman in need. Was it the birth of your child? The explosion of a sunset? The impassioned sermon that moved all? The dull sermon that moved none but you? As the new year begins, decide to make this year the year you get serious about your faith. God already is. If you have a story, don’t keep it to yourself. If you don’t have a story of conversion, ask God to give you one. That is a request God will love to grant. 
 
What story of faith or growth in faith did these stories remind you of? Write it down and write down someone you want to tell the story to today. Think of one way to go deeper in your faith as a gift to yourself and God this New Year. Say a prayer for yourself and someone else who needs to grow in their faith. May you have a happy New Year. 

To see Al’s new book on Amazon, search My Faith Journal by Albert Earley. It is a daily devotional that is a compilation of 35 years of Al’s articles. Take the One-Year Challenge that will change your faith.
