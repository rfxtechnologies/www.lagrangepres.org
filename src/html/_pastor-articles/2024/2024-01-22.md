---
layout: "articles.njk"
title: "How to Win an Argument and Crush Your Opponent"
date: "01-22-2024"
slug: "how-to-win-an-argument"
summary: "Jesus’ words, “Father, forgive them for they know not what they do,” established that our faith is not a faith of revenge."
---

Jesus’ words, “Father, forgive them for they know not what they do,” established that our faith is not a faith of revenge.  Stephen, the first Christian martyr, looked up to heaven as the stones were crushing his body and prayed, “Lord, do not hold this sin against them” (Acts 7:60).  Since the death of Jesus Christ 2000 years ago, 43 million Christians have become martyrs.  Over 50% of these were in the last century alone.  More than 200 million Christians face persecution each day, 60% of whom are children (information taken from the World Evangelical Encyclopedia).  

The early Church Father, Tertullian, wrote that, “The blood of the martyrs is the seed of the church.” In a very real sense this is true. In spite of vicious opposition, the church has always grown and flourished during times of persecution.  People are always drawn to someone who believes in something worth dying for.  

This is a far cry from the vitriol that captures Christians today when things don’t go the way they think the Bible teaches.  It is so sad to me that Christians will use any public and social media forum to blast those that disagree with them, condemning them to hell, calling them names, etc.  I can find no Biblical examples of this.  God rarely, if ever, uses angry, vengeful, out of control, Bible thumping Christians to build up His kingdom.  

As Christians, we need to model II Timothy 2:24-26, which says, “And the Lord’s servant must not be quarrelsome but must be kind to everyone, able to teach, not resentful.  Opponents must be gently instructed, in the hope that God will grant them repentance leading them to a knowledge of the truth, and that they will come to their senses and escape from the trap of the devil, who has taken them captive to do his will.”

I found an intriguing article at Faithstreet.com called, “The Five Things to Remember When Standing up for the Faith.”  I think the problem of Christians being a bad witness of our faith because of the way we argue is important, and I want to take two weeks to look more deeply.

First, it can be extremely helpful to define terms, like what is someone’s definition of God.  Simply asking someone who God is to them, can be a great way to open the doors to a good discussion about what is important in life.  People have lots of ideas about God.  Being sensitive to someone else’s understanding of God, even if we disagree, can go a long way to opening the possibility of us hearing one another.  Paul puts it this way, “Though I am free and belong to no one, I have made myself a slave to everyone, to win as many as possible.  I do all this for the sake of the gospel, that I may share in its blessings” (I Corinthians 9:19 & 23).  Is our focus to be right or righteous? 

A second thing to remember is not to treat your conversation as a confrontation.  Treat it as a collaborative effort to get closer to truth.  As Philip Yancey said, “No one ever converted to Christianity because they lost the argument.”  Do you think if you are able to just explain your arguments really well then your excellent reasoning will compel someone to accept your point of view?  

A Christian friend of mine was a missionary to Afghanistan for many decades.  Occasionally, he had a chance to verbally witness his faith to the Muslims he met, but usually he ministered to their many needs, something he was uniquely gifted to be able to do.  While bringing medical care to a remote village, the chief of the tribe called him aside.  From a carefully hidden place, he pulled out a Bible and started asking about it.  My friend knew God had been working for over ten years to bring this Muslim leader to this place.  A powerful conversation about Jesus occurred.  The man was not ready to face all the complications and dangers he might encounter if he gave his life to Christ, but he would talk to my friend more whenever he visited the remote village.  

Do you find yourself getting angry when you read something on social media that seems, stupid, uneducated, or unbiblical?  Do you send an incendiary bomb out to blow up the thinking of the other person?  How many people have changed their minds after those bombs go off?  Have you ever thought that maybe social media is simply a bad place to fight over things?  I hope you will look forward to more thoughts on “How to Win an Argument and Crush Your Opponent” next week.   