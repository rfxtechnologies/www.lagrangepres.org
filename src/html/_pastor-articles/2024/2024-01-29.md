---
layout: "articles.njk"
title: "How to Win an Argument and Crush Your Opponent: Part II"
date: "01-29-2024"
slug: "how-to-win-an-argument-part-ii"
summary: "When two people are arguing, verbally to the death, we need an extreme example to remind us that we are called to live higher. Our faith in Jesus Christ, the Prince of Peace demands it."
---
If you haven't figured it out, this two-part series is not about winning and crushing your opponents.  It is about how Christians engage those who disagree with us in a Christ-like manner.  Last week I offered two things to keep in mind.  First, seek to understand what the other person believes about God, and be respectful of those beliefs even if you disagree.  Second, decide to have conversations, not confrontations.

Third, ask questions instead of making statements.  When we ask questions, we communicate an interest in the other person.  This can open the doors to finding common ground.  You may find that your differences are not as big as you think.  It can also earn you the right to be heard.  It shows respect rather than being adversarial.  When Jesus met Bartimaeus, the blind man, He asked Bartimaeus what he could do for him.  Bart may have wanted alms, or any number of other things in life.  He wanted to see, told Jesus, and Jesus healed his blindness (Mark 10:46-52).
     
The fourth thing I lift up is something you need to ask yourself, especially if you find yourself in “knock down, drag out” verbal fights with others.  Is it more important to you to be RIGHT or RIGHTEOUS?  Of course, we should prefer to be righteous, but having a preference and actually being righteous are two very different things.  A simple test to find out which you are is to decide whether the other person leaves angry or not.  If they are angry then you prefer being right over righteous.  It is very hard to make an honest self-evaluation of ourselves.  On this, you should want to be brutally honest.
     
Fifth, A joyful, Christ-filled life is a far more powerful argument than anything else.  Paul guides us here in Philippians 4:4-5, “Rejoice in the Lord always. I will say it again: Rejoice!  Let your gentleness be evident to all.”  It can be very attractive to the other person if they encounter love, gentleness, and respect on something you disagree on, rather than anger, meanness, and disrespect.  Isn’t it always intriguing when someone we argue with continues to have a joyful countenance?
     
Finally, actions can often speak louder than words.  The phrase "talk the talk, and walk the walk" is an idiomatic expression that emphasizes the importance of matching one's actions with their words. It means that it's not enough to just speak about doing something or to make promises; one must also demonstrate those intentions through action.  Are our actions congruent with the things we say we believe?
     
This story comes from the book, Christian Martyrs of the World by John Foxe (pp. 26-27).  It is the story of the hermit, Telemachus.  Rome was celebrating its temporary victory over Alaric the Goth in its usual manner, by watching gladiators fight to the death in the arena.  Suddenly, there was an interruption. A rudely clad robed figure boldly leaped down into the arena. Telemachus was a hermit, one of the people who devoted themselves to a holy life of prayer and self-denial and kept themselves apart from the wicked life of Rome. Although few of the Roman citizens followed their example, most of them had great respect for these hermits.
     
Without hesitating an instant, Telemachus advanced upon the two gladiators who were engaged in their life-and-death battle. Laying a hand on one of them, he sternly reproved him for shedding innocent blood, and then, turning toward the thousands of angry faces around him, called to them, "Do not repay God’s mercy in turning away the swords of your enemies by murdering each other!"
     
Angry shouts drowned out his voice. "This is no place for preaching! On with the combat!" Pushing Telemachus aside, the two gladiators prepared to continue their combat, but Telemachus stepped between them. Enraged at the interference of an outsider with their chosen vocation, the gladiators turned on Telemachus and stabbed him to death.  The crowd fell silent, shocked by the death of this holy man, but his death had not been in vain, for from that day on, no more gladiators ever went into combat in the Colosseum.
     
This is certainly an extreme example, but then when two people are arguing, verbally to the death, we need an extreme example to remind us that we are called to live higher.  Our faith in Jesus Christ, the Prince of Peace demands it.
     
When you argue with others, is your faith in Jesus Christ evident?  Have you ever asked yourself if you would rather be right or righteous?  Which of the above suggestions would help you in debates the most?  (To find out more about Al Earley or read previous articles see, www.lagrangepres.org). 
