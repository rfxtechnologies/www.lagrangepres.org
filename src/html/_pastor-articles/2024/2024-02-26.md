---
layout: "articles.njk"
title: "Prayer and the Three Temptations"
date: "02-26-2024"
slug: "prayer-and-three-temptations"
summary: "Do you ever wonder what Jesus prayed about?  Think about the claim we make about him.  Jesus is the incarnate one, fully God and fully human.  This is certainly a mystery of creation.  How can Jesus retain any semblance of humanity in light of his divinity? "
---

It is to Luke's gospel that we owe most of our insights into Jesus' prayer life.  The first-time Jesus is shown as a man of prayer is after his baptism.  As he prayed, the Spirit descended upon him like a dove, and God said, "This is my Son, the beloved, with you I am well pleased" (Luke 3:21-22).

Do you ever wonder what Jesus prayed about?  Think about the claim we make about him.  Jesus is the incarnate one, fully God and fully human.  This is certainly a mystery of creation.  How can Jesus retain any semblance of humanity in light of his divinity?  Though the incarnation, God becoming man, is a mystery, the Gospels show us that Jesus needed to pray to receive guidance about the will of God.  As a human, He was limited in His understanding of where God wanted Him to be, who God wanted Him to meet, and what God wanted Him to do.  In prayer Jesus lived out the radical obedience God desires us to learn to live.  

We get another glimpse of Jesus' humanity in the temptations in the wilderness (Luke 4:1-13).

First, the devil tempts Jesus to turn the stones into bread, that he might feed himself, that he might use his divine power to satisfy his human need.  But Jesus must not yield to such temptation, and he turns to scripture to defend Himself against satan’s temptations.  He tells satan, “Man cannot live on bread alone” (Deuteronomy 8:3).

Next, the devil tempts Jesus with the gift of all the kingdoms of the world.  It is a tempting gift, the gift of world domination, and it is easy for the Devil to offer it to Jesus, because it is not his to give away.  But it will continue to be a temptation for Jesus throughout his ministry, for he clearly has the charisma and divine power to gain control of the kingdoms of this world.  Jesus will visit this temptation again when he parades into Jerusalem on that Palm Sunday.  And this is a very human desire, the gaining of honor and glory.  But Jesus must not yield to such a temptation, if he is to reveal our salvation and the power of love in the Kingdom of God.  Instead, he must reveal that you and I, as we are tempted by the pursuit of power and glory, are to worship God and serve Him only (Deuteronomy 6:13).

The third temptation the devil sets before Jesus is, "If you are the Son of God, throw yourself down from the pinnacle of the temple and see if God's angels rescue you."  Will he use his divine power to prove he is the Son of God and deny his humanity?  Enduring the agony of the cross, the devil would tempt Jesus again to rescue himself.  Another very human desire we all have is to be gods, to be free of limitations of our humanity, and to control our future.  If Jesus had destroyed his humanity, we would have been left without the revelation of how we find forgiveness from sin and temptation through the power He offers us when we believe in Him.  Jesus defeats this temptation saying, “Do not put the Lord your God to the test” (Luke 4:12, Deuteronomy 6:16).

I believe Jesus prayed often to understand who he was as the incarnate one: fully human and fully divine.  He had to, for as Luke says, “The devil merely departed the wilderness until an opportune time” (Luke 4:13).  And we can see throughout Jesus' ministry he was tempted to deny his full humanity and establish himself as an earthly king.  Jesus had to pray to fully understand the will of God.

And we are tempted as well, you and I, who are fully human with the power of the Holy Spirit in our hearts.  We are tempted to use our wealth to satisfy all our physical needs, forgetting we cannot live by bread alone.  We are tempted to use our talents for our own power, personal gain, and glory, forgetting we are to worship the Lord our God and serve Him only.  And we are tempted to believe our worship of God will keep our lives free from trial and trouble, forgetting that true faith does not test the Lord our God.

How tempted are you by your wealth?  Do you live as if you don’t need God because you can buy everything you need to be comfortable?  Do you tithe?  When you do something well do you accept the praise and applause?  How do you point people to God when you use your God given gifts to accomplish things?  How important is worship of God to you?  Do you worship with a congregation once a week?  How do you worship God every day?


