---
layout: "articles.njk"
title: "When Words Cannot Be Found"
date: "04-15-2024"
slug: "when-words-cannot-be-found"
summary: "There are times in our lives when words cannot fully capture an experience we have.  Can words capture the grandeur of the Grand Canyon, the miracle of childbirth or describe a religious experience to others?"
---

There are times in our lives when words cannot fully capture an experience we have.  Can words capture the grandeur of the Grand Canyon, the miracle of childbirth or describe a religious experience to others?  It can be hard to tell God how overwhelming He can be.  There are times when we experience life’s hardest challenges, and we just aren’t sure what to ask from God to get us through.

Some people are afraid to pray to God, not only in public, but also alone, because they fear their words will be wrong.  Our human words are so limited when it comes to talking about divine things, and talking to the divine.  Have you ever felt your prayers sounded so common, so simple, so insufficient?  

I think this is why so many people use the word “just” when they pray.  For example, “Dear God, we just ask you to be present with us and bless our time of worship.”  “Just” does not seem like an appropriate word here.  First, we are asking the creator of the universe to be present with us.  Either ask boldly for God’s presence, or don’t ask at all.  Second, God wants us to ask this of Him because He desires to have a personal relationship with us, so, He does not want us to “just” ask anything, but instead to ask boldly from Him, the giver of all good gifts (James 1:17).

In Luke 10:21-22, Jesus prays a prayer that can be difficult to understand.  He prays, "I thank you, Father, Lord of heaven and earth, because you have hidden these things from the wise and intelligent, and have revealed them to infants; yes Father, for such was your gracious will.  All things have been handed over to me by my Father; and no one knows who the Son is except the Father, or who the Father is except the Son and anyone to whom the Son chooses to reveal him."

I wondered why Luke remembered this prayer, for it gives thanks that some don't understand God, and includes difficult language of the Father knowing the son, and vice versa.  Perhaps it is true that sometimes words cannot be found. But, as I looked at this prayer in the context of its specific situation, it becomes meaningful.  

Jesus had just sent seventy disciples to go out on their first missionary journey.  They have had a powerful experience.  They healed people, saw people come to faith, cast out demons and were welcomed and fed in people’s homes.  And as they returned and told their stories, the excitement grew.

Jesus is excited, too!  "I saw Satan fall from heaven like a flash of lightning.  See, I have given you authority to tread on snakes and scorpions, and over all the power of the enemy; and nothing will hurt you.  Nevertheless, do not rejoice at this, that the spirits submit to you, but rejoice that your names are written in heaven" (Luke 10:18-20).  If you read the prayer above again, you will see that its meaning increases as Jesus gives glory to God. 

When we are struggling with the words of our prayers, we must remember that those prayers include more than words; they include experiences in life and experiences of God's power.  A simple prayer like, "Dear God, thank you for the sunshine of Spring.  Amen," carries with it the long, cloudy winter we've experienced, memories of the sun's rays warming our skin, and how beautiful the spring flowers are as they burst forth.

The words are still insufficient, but the prayer encompasses our mental memories, our senses, our expectations and our thanks to God.  Paul has a beautiful explanation of how God helps us to give thanks when words cannot be found.  "Likewise the Spirit helps us in our weakness; for we do not know how to pray as we ought.  But that very Spirit intercedes with sighs too deep for words; And God, who searches the heart, knows what is in the mind of the Spirit" (Romans 8:26-27).

Do not be concerned about how good your words sound, but pray from the heart, from your soul, and know God will search you and know you and your prayer.  More than anything, God wants a relationship with you.  God wants your heart and soul, given to Him.  God wants you to come to Him in prayer and tell Him everything.  

When was the last time you and God had a good talk?  Do you use the word “just” when you pray?  Is that really the word you want to use?  Why or why not?  Can you remember the boldest prayers you have lifted up to God?  What were the circumstances, and how did God respond to your prayers?