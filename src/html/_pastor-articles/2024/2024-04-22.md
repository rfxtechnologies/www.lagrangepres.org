---
layout: "articles.njk"
title: "Seeing Another’s Point of View"
date: "04-22-2024"
slug: "seeing-anothers-point-of-view"
summary: "Think about the truths and beliefs you believe are worth fighting over. There certainly are some, but not as many things as we might think."
---
Linda tells about a terrible day when she had an extremely painful toothache and a fight with a friend the same day.  Her friend's words stung her, "The trouble with you is that you won't put yourself in my place. Can't you see things from my point of view?"

Linda shook her head stubbornly, as her tooth ached.  She hoped to wait until her dentist returned from vacation, but the pain was too much.  She called many dentists, but none was able to see her that day, except one.  She wondered if the reason might be that he was incompetent.  The assistant noticed her fears as things were prepared for the dentist, and Linda shared her fear of the dentist's ability.  The assistant said, "He's very good because he really tries to see things from the patient's point of view."
     
Linda thought to herself, "There it was, that phrase again.  What did she mean?"
     
"Relax," she said, "And enjoy the art work."
     
Puzzled, Linda looked around at the bare walls.  Then the chair went back.  Suddenly she smiled.  There was a beautiful travel poster, right where she could enjoy it - on the ceiling.
     
It is not easy for any of us to see a problem or situation from another person's point of view.  Often arguments that separate us from friends and loved ones arise from our inability to look at something from any other perspective but our own.  This is even more true today than it has ever been in my lifetime.  The emergence of the “Cancel Culture” is a terrible thing that threatens to undermine our way of life in free America.  Our Founding Fathers considered freedom of speech to be one of the most important freedoms we need to preserve to remain free as a society.  
     
Throughout my life I have seen many bad ideas and opinions come and go, but these were not the evil part of a bad idea or opinion.  Once those ideas and opinions were in the public domain people could debate them.  Sometimes it might take time, and even be very costly, but eventually the bad ideas and opinions have always been exposed.  The ideas of the Ku Klux Klan in the 1960’s and 1970’s was just such an example.  For decades, the bad ideas of the KKK dominated our culture.  Many people died at the hands of the hatred they fostered.  But over time, the lies they spread were exposed, until the voice of the KKK has become meaningless, except in the smallest of circles. They planned a march in the 1980’s through some community of people they hated.  Should we use the force of the state to mandate they be silenced?  In the end, they were allowed to march, and spread their lies and hatred, and people stopped listening to them completely.  Freedom of speech opened the doors for truth to be seen and finally win the day.  
     
In Luke 6:6-11 Jesus heals the withered hand of a man on the Sabbath in front of some Pharisees.  From their point of view this healing is evil.  The Sabbath Laws were set in stone, and breaking them was a sin against God.  It so angered them that they began the plot that would eventually kill Jesus.  
     
Their interpretation of Jewish law blinded them to the good Jesus did for this man.  Jesus, speaking with the authority of God, announced in Mark 2:27, “The Sabbath was made for man, not man for the Sabbath.”  Throughout his ministry, Jesus would do good whenever He could, whether it was the Sabbath or not.  I like Jesus’ point of view.  
     
Think about the truths and beliefs you believe are worth fighting over.  There certainly are some, but not as many things as we might think.  I would fight for the safety of my family, my good name, the right and ability of others to live a just and free life, especially their freedom of speech, and, most importantly, for the right to express my faith in Jesus Christ.  Most other things are better left as discussions, debates, and chances to share with and learn from other people.  What do you think of my point of view?
     
Have you ever tried to cancel someone?  Do people respond better to open debate and dialogue, or being bullied into silence?  Are you willing to fight for another’s freedom of speech even if you think they are wrong?  Is there an opinion you once held that you knew was right, but then as you grew older you changed your mind, and realized you weren’t as right as you thought?  I hope you will learn to listen to people, and look at things from their point of view.  (To find out more about Al Earley or read previous articles see, www.lagrangepres.org). 
