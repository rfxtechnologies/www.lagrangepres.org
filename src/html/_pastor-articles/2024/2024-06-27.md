---
layout: "articles.njk"
title: "Whose Time Is It, Yours or God’s?"
date: "06-27-2024"
slug: "whose-time-is-it"
summary: "The book of Exodus tells the story of Moses as he leads the Hebrew escape from Egyptian slavery, and helps them to understand their role as God's chosen people.  They must also cope with the harsh reality of survival in the desert."
---

The book of Exodus tells the story of Moses as he leads the Hebrew escape from Egyptian slavery, and helps them to understand their role as God's chosen people.  They must also cope with the harsh reality of survival in the desert.  God reveals to them the importance of holy time in their survival, as Moses says, "This is what the Lord has commanded: 'Tomorrow is a day of solemn rest, a holy Sabbath to the Lord...'" (Exodus 16:23).  Just as the Hebrews needed holy time to cope with the harsh reality of desert life, so do we, as we live in the complex high-tech age.

To illustrate my point, think about the month of May.  How many of you struggled through the busy schedule of Mother’s Day, Memorial Day, graduation parties, end of school responsibilities, and other activities often feeling exhausted?  Did you ever think things would slow down in June?  It sounds good, but have things slowed down any?  I have found my June to be just as busy as every other month.

Many of us have busy schedules as we try to balance careers, volunteer work, parenting, and looking after parents and relatives.  This leaves less and less time that we can call holy time: that is, time for us to talk to God in prayer each day and worship God each week.  

The thing that makes this article so difficult is that many of the things people do are important to the community, the church, family, friends, ourselves, and God.  Many of the activities we participate in are ways we put our faith into action.  Even so, most of us will agree, it is time to closely examine our calendars, computerized date books, stop watches, and wrist alarms, and cell phones, and decide if our schedules control us, or we control them, and where does God fit into our lives?

At this point I want to say, "This is not a go-to-church article, but an article on the stewardship of time."  For all we have is a precious gift from God, including the gift of time, and the Bible has some things to say about the use of time and setting aside "holy time."  Nowhere does the Bible say, "Go to church every Sunday," but it does say clearly, we must have time for God.  In six days, God created the heavens and the earth, and on the seventh day God rested.  If God needed a day of rest, what does that say about us?  In their wilderness wanderings, the Hebrews believed that it was God's will they rest on the seventh day.  The fourth of the Ten Commandments calls us to keep the Sabbath holy.  Jesus went to synagogue regularly and introduced regular prayer as an important part of holy time with God.  The early church worshipped regularly on Sunday, the day of resurrection.  For millennium worshipping with a community of faith has been the way faithful believers have found they will worship God regularly and get Sabbath rest.  If you don’t think this is the way God is calling you to keep the Sabbath holy and get spiritual and physical rest then you must think carefully through what your plan is, and why it will honor God.

No matter how important our daily rounds may be, we need holy time each week.  We need time to spiritually recuperate, rejuvenate our faith, give glory to God, and receive God's grace.

It has only been since 1961 that our culture has tried to thrive without holy time.  This is when the Supreme Court said K-Mart and other businesses could be open on Sundays.  

Have sixty years of being able to shop on Sundays improved our spiritual lives, or proved we have a need for holy time?

Whether we take a quiet walk in the woods, take time to read some scripture, hum a hymn and say thanks to God at home, or get to church each week, we need holy time.  The Bible tells us clearly, we need this.

Do you believe worship of God is at the center of your life?  When was the last time you set aside time to worship God?  Does this happen weekly, or every now and then?  Do you need the discipline to gather with a congregation to worship God regularly?  What do you do to take a Sabbath rest each week?  Do you get enough rest?  Let us always affirm the need for Holy Time, that our light of Jesus Christ will always shine bright for all to see.  I have found the only way to do this for me is through a commitment to daily prayer and worshiping with my congregation each week.  What is your plan to worship God?  