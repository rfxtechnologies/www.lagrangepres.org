---
layout: "articles.njk"
title: "How Do We Build New Relationships with Our Siblings?"
date: "08-08-2024"
slug: "new-relationships-with-siblings"
summary: "Little sister had this knack for dating losers. The big brothers had this desire to protect their sister from these losers. She knew her brothers were right, but it made her mad."
---

Little sister had this knack for dating losers. The big brothers had this desire to protect their sister from these losers. She knew her brothers were right, but it made her mad. As the string of losers got longer and longer, the relationships with her brothers became more and more tense. What was meant as a sign of love became a symbol of control and drove a painful wedge between the sister and her brothers. 

Do you want to start healing you’re the relationship with a brother or sister? It seems like there is no way to get started, especially if the last time you spoke was years ago, and at best it was strained or perhaps it was an all-out verbal battle that left you wondering if you would ever speak again. You probably will but, it will likely be at difficult time, like a funeral for one of your parents. I see this all of the time and it is so sad. The siblings want to celebrate their Dad’s life, but they don’t like being together. Is there anything that can be done in these situations?

I have found one thing that works, and it was something that God revealed to me which helped make me a better pastor in times that I struggle with a family in need. Occasionally, siblings can reconcile during difficult times like a funeral, but usually they just endure it and get away from each other. The time to start the healing process is when things are going well in your lives, or at least relatively quiet, instead of during a difficult situation.

Call your sibling on the phone regularly. That is it! Call at least three or four times a month with no agenda except to say, “Hello,” and see how they are doing. Proverbs 25:11 says, “A word aptly spoken is like apples of gold in settings of silver.” It is time you shared some apples of gold with someone you are supposed to love.

The key is not to have an agenda. It’s usually best to steer clear of politics, religion, and life choice discussions unless these are topics you have been able to have in the past without arguing. If an argument begins, drop the discussion and simply say, “I don’t want to argue with you about that, let’s talk about something else.” 

Especially stay off hot button topics like life choices. Remember, in the first week of this series, I shared “Earley’s Rule for Relationships.” It states that you only tell a loved one that you disagree with their choice one time, if you have to tell them at all. Once you have told them, they know you disagree with their choices. Let it go. 

What do you talk about? Talk about the children (and don’t go overboard bragging about yours), family memories, how the job is going, sports and their health. These can all be safe topics. Decide to be interested in their life partner. You don’t have to like the person to care about them. If you haven’t began praying for their life partner, now is a good time to begin. You will never fix your brother’s strained marriage or live-in situation by being rude about it. Showing love and support makes it far more likely that he will reach out for support, or even guidance. 

How long does this take? Does it matter? Decide that you are going to love your siblings and get back into their lives. It could take years before you are able to talk freely but, it will be worth it. The benefits bring joy back into your family. You may even find that, in a time of struggle, your brother or sister may ask you for advice. Then, without reverting back to your old controlling ways, you can share a heart to heart conversation with a sibling that you truly love and help them find the best for their life.

If your Mom or Dad died today, could you work well with all of your siblings to have a great celebration of their life? When was the last time you spoke to your sibling? What went wrong with that conversation? Can you make changes and begin talking to your sibling again without fighting? If not, who can you talk to that can help you get the right mindset? Have you prayed about it?

Life is too short to live fighting with people you are supposed to love. I hope you will pray for God to help you heal those sibling relationships, and experience the joy of sharing the rest of your life with them.
