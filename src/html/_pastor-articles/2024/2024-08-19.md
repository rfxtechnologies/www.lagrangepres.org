---
layout: "articles.njk"
title: "The Pharisee and the Tax Collector"
date: "08-19-2024"
slug: "the-pharisee-and-the-tax-collector"
summary: "Jesus gives examples of two Bible prayers in a parable in Luke 18:9-14. We read, “Two men went up to the temple to pray, one a Pharisee and the other a tax collector. The Pharisee stood by himself and prayed: ‘God, I thank you that I am not like other people—robbers, evildoers, adulterers—or even like this tax collector. I fast twice a week and give a tenth of all I get.’ But the tax collector stood at a distance. He would not even look up to heaven, but beat his breast and said, ‘God, have mercy on me, a sinner.’”"
---

Jesus gives examples of two Bible prayers in a parable in Luke 18:9-14. We read, “Two men went up to the temple to pray, one a Pharisee and the other a tax collector. The Pharisee stood by himself and prayed: ‘God, I thank you that I am not like other people—robbers, evildoers, adulterers—or even like this tax collector. I fast twice a week and give a tenth of all I get.’ But the tax collector stood at a distance. He would not even look up to heaven, but beat his breast and said, ‘God, have mercy on me, a sinner.’”

Let’s get something straight; the Pharisee was a very good man who prayed a very bad prayer, and the tax collector was a very bad man who prayed a very good prayer. Which are you more like? Are you like the Pharisee? Do you fast twice a week? Do you tithe? Do you refrain from stealing, are you faithful to your spouse and are you trustworthy? Or are you like the tax collector? Do you cheat other people, do you take an extra cut for yourself from your boss and do you live a life that brings pain and struggle to others?

Let’s break it down further, Jesus didn’t tell this parable for us to consider how good we are. He told it so we can know how bad we are. Verse 9 sets the stage for this parable, for he was telling it to people who were confident of their own righteousness. The Pharisee was described as a person who trusted in himself as righteous. When he prayed to God, he focused only on what was good in his life. As a devout follower of God, fasting and tithing are not noteworthy. These are expected signs of obedience and commitment. Humility is always noteworthy.

So, the question is not do you live a respectable life. The question is, “Can you go to God and honestly ask God to forgive you for your sins, and not be generic about what your sins are?” That is the one good thing this tax collector did. He was humble in the presence of God.

Humility is one of those things that is hard to define, but we know it when we see it. For the Christian, I think a simple definition of humility is to focus on serving Jesus first, others second and ourselves third. If Jesus is first, then we will get the other two right.

A football player had lost a great football game and he was speaking on the telephone and he said their team would have won this game had they only had a blocking back. His little son heard him and said, “Daddy, I want to grow up to be a blocking back.” Then the boy asked, "Daddy, what is a blocking back?" The father said it is someone who does all the work but does not get any of the credit. That is a good example of humility, seeking to do the work for Jesus Christ, but not looking for the credit.

It is important to remember humility is not weakness. Humility is Jesus riding into Jerusalem on the back of a donkey. Everyone sees a war horse as they wave their palm branches in hopes that Jesus will lead a rebellion against the hated Romans. They see their king throwing out Pontius Pilate. Jesus sees the road to betrayal. He is riding the road to a mock trial. He sees a weak governor who washes his hands of the death of an innocent man. He is riding to Golgotha. He is riding to the cross to die for our sins and our salvation. He is doing this courageous act to save us from our sins because He is humbly doing what His Father, God Almighty, told Him to do. He is obediently humbling Himself by letting His creation kill Him on the cross. This is the perfect example of what humility is.

Have you ever prayed a prayer of humility? Look at the parable again and write or say a prayer of humility to God. The tax collector knows how much he needs God, forgiveness, and salvation. Do you know what sins you struggle with? List as many as you can (at least five). Do you know why you need a savior? What is the humblest thing you have ever done?
