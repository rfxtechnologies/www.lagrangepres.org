---
layout: "articles.njk"
title: "If You Follow Your Heart Everything Will Not be Okay"
date: "09-26-2024"
slug: "if-you-follow-your-heart-everything-will-not-be-okay"
summary: "Can you think of a time you followed your heart and knew your heart was right, but it turned out badly? Do you know how to discern the will of God when you have a sense you need to stop listening to your heart? Is there someone in your life that knows God with a mature faith, that can help you when you are tempted to follow your heart?"
---

A contributor to the Huffington Post, Dr. Nikki Martinez, wrote an article “10 Reasons to Follow Your Heart” (posted Feb. 9, 2016). Some of the highlights include; When you follow your heart, you cease having regrets, you gain a newfound respect for yourself and from those you admire, you get to know who you really are and what really matters to you, and you ensure that you are on the right and true path for yourself. These four and all the others couldn’t be more wrong.

The movie industry is captivated by the wisdom that if you follow your heart everything will be okay. All the Disney princesses tell us in all kinds of victorious ways to follow our hearts. Almost every Disney movie finds a way to insert this “wise” admonition.

The truth is no one lies to us more than our own hearts. No one! If our hearts are compasses, they are Jack Sparrow compasses. These compasses don’t tell us the truth, they tell us what we want. It will only point us to what our heart wants most. What did Jack, the no good, lovable lead pirate in “Pirates of the Caribbean,’’ find with his compass? He only found his pirate ship and lots of cursed treasure.

In Jeremiah 17:9 we read, “The heart is deceitful above all things and beyond cure. Who can understand it?” Contrary to the writings of Dr. Martinez, the heart teaches us that reality ought to serve our desires. We learn to think the best of ourselves and those who build us up, and the worst of those we don’t like. Following our heart often leads to adultery, fornication, divorce, and shattered marriages and families. Too many men follow their heart through a mid-life crisis, and trade in the bride of their youth for a new, youthful bride. Jesus says, “For out of the heart come evil thoughts—murder, adultery, sexual immorality, theft, false testimony, slander” (Matthew 15:19).

Jeremiah prophesies great wisdom about how we should deal with our heart. Just before he tells us not to follow our heart, he writes, “Blessed is the man who trusts in the Lord, whose trust is in the Lord” (Jeremiah 17:7). Our heart was never designed by God to be followed, but to be led. God designed us to let Him lead our heart.

One of the most powerful verses about our hearts can be found in Jeremiah 29:11-13, “For I know the plans I have for you,” declares the Lord, “plans to prosper you and not to harm you, plans to give you hope and a future. Then you will call on me and come and pray to me, and I will listen to you. You will seek me and find me when you seek me with all your heart.” The guidance is clear not to follow our heart, but to seek God with all our heart. It is God who redeems our hearts and our lives when we go astray, and become consumed with momentary pleasures and self-centered dreams and ambitions.

Note that Jesus never said, “Do not let your hearts be troubled, but follow your heart and everything will be okay.” In John 14:1, He did say, “Do not let your hearts be troubled. You believe in God; believe also in me.” All of us know what it is to have troubled hearts. These are the times to know that our heart is deceitful above all things, and turn to God and seek His will for our lives.

Just before Jesus’ crucifixion He went to the Garden of Gethsemane to pray. He told God His heart’s desire. Would God take the cup of death from Him? If He had followed His heart the world would be much different than it is today. God revealed His will that this was the time for Jesus to submit to the authorities, and let them kill Jesus for the sins of the world. In obedience, He was arrested in the Garden, ready to die for you and for me. Jesus is the greatest gift we have ever been given. Thanks be to God!

Can you think of a time you followed your heart and knew your heart was right, but it turned out badly? Do you know how to discern the will of God when you have a sense you need to stop listening to your heart? Is there someone in your life that knows God with a mature faith, that can help you when you are tempted to follow your heart? May you always let God lead your heart down His path, so you experience His blessings. To God be the Glory!
