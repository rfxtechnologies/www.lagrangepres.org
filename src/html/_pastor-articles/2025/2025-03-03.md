---
layout: "articles.njk"
title: "What is Heaven Like?"
date: "03-03-2025"
slug: "what-is-heaven-like"
summary: "There are lots of jokes about heaven on the internet. Perhaps because the Bible is our best source of information and it is not overly concerned about telling us about heaven. Also, the Bible must use language we can understand, so it is limited in how to explain the absolute glory of the perfect place heaven is. Today, I am going to share an overview of some of what the Bible teaches us about heaven that people are most curious about."
---

Two Trump supporters die and go to heaven. God meets them at the Pearly Gates and asks them if they have any questions. One of them says, “Yes, what were the real results of the 2020 election and who was behind the fraud?” God says, "My son, there was no fraud. Biden won the electoral college fair and square, 306 to 232.”

After a few seconds of stunned silence, the one guy turns to the other and whispers, “This goes higher up than we thought.”

There are lots of jokes about heaven on the internet. Perhaps because the Bible is our best source of information and it is not overly concerned about telling us about heaven. Also, the Bible must use language we can understand, so it is limited in how to explain the absolute glory of the perfect place heaven is. Today, I am going to share an overview of some of what the Bible teaches us about heaven that people are most curious about.

Is heaven a real place? Twice in three verses Jesus calls heaven a place. Listen to the words of Jesus on the night before he was crucified, “In my Father's house are many rooms; if it were not so, I would have told you. I am going there to prepare a place for you. And if I go and prepare a place for you, I will come back and take you to be with me that you also may be where I am” (John 14:1-3).

What is heaven like? Here are seven biblical facts about heaven. Heaven is: 1) God's dwelling place (Psalms 33:13). 2) Where Christ is today (Acts 1:11). 3) Where Christians go when they die (Philippians 1:21-23). 4) The Father's house (John 14:2). 5) We will not be sick, age, or cry(Revelation 21:4). 6) Our bodies will be Resurrection Bodies fitted for the resurrection life (I Corinthians 15). 7) Streets are paved with gold, the gates are made of pearl and the walls are made of precious jewels. (Revelation 21)

Will we recognize each other in heaven? There is no scripture reference that explicitly states that we will recognize each other in heaven, however, the Bible implies in many ways that we will be able to recognize each other. For example, Jesus tells a parable about a poor man named Lazarus who goes to heaven at death. A rich man who ignored Lazarus’ needs in this life recognizes him at his judgment (Luke 16:19-31).

Also, people who have had Near Death Experiences commonly remember meeting loved ones as they cross over from this life to the next. These loved ones seem to be there to welcome them to heaven, or sometimes to tell them their time on earth is not done, and they must return. All these taken together paint a fairly clear picture that we will be able to recognize each other.

What will we do in heaven? Again, the Bible doesn't tell us everything we would like to know, but it is unlikely it will be a place where we sit around on a cloud polishing our halo and playing a harp. No, we'll all be too busy for that. In Genesis 2 the Garden of Eden is described as an adventure land, and that is heaven on earth.

I like this simple description of heaven as a place where we will: Worship without distraction, Serve without exhaustion, Fellowship without fear, Learn without fatigue, and Rest without boredom (taken from a sermon by David Burns, Minister at the Homer Church of Christ, called "Heaven is a Wonderful Place," February 25, 1996.)

Finally, life on earth will be preparation for eternity in heaven. The Angel reveals to the Apostle John that the things we do on earth matter to God and go with us into eternity. We read in Revelation 14:13, “Blessed are the dead who die in the Lord from now on… they will rest from their labor, for their deeds will follow them.”

The Parable of the Talents, (Matthew 25:14-30), is about what it is like in the Kingdom of Heaven. When one of the servants does well, the Master (God) praises him with these words, “Well done, good and faithful servant! You have been faithful with a few things; I will put you in charge of many things. Come and share your master’s happiness!” This parable reinforces that we continue to grow in our ability to serve God.

Do you live with great expectation of what heaven will be like? Why do you think the God reveals a limited amount of information about heaven in the Bible? To God be the glory!

(To learn more about Al Earley or read previous articles, see www.lagrangepres.org. You can purchase my book, <u>My Faith Journal</u>, at Amazon.com, a compilation of 366 articles as a daily devotional).

