{% set canonical = domain + '/missions/local' %}
{% set pageName = "Local Missions" %}

{% extends '_layouts/default.njk' %}

{% block main %}
    <section id="dark-content">
        <h1 class="section-title large">
            <p>Local Missions</p>
        </h1>

        <div class="opening-quote">
            <p>Then Jesus said to his disciples, “The harvest is plentiful but the workers are few. Ask the Lord of the harvest, therefore, to send out workers into his harvest field.”</p>

            <strong>Matthew 9:37-38</strong>
        </div>

        <div class="gcontent">
            <h3>COPPERSTONE TRAILER PARK MINISTRIES</h3>

            <p>Our congregation has an active ministry to this LaGrange trailer park. We provide transportation, financial assistance, Bible studies, and Children’s Christian Education through Kids of the Kingdom each Sunday, Sunday School, and Vacation Bible School. Our goal is to strengthen the families of this community through the power of Jesus Christ in their lives.</p>

            <h3>KIDS OF THE KINGDOM</h3>

            <p>This new ministry called "Kids of the Kingdom" includes all ministry to children: Sunday school programs, youth programs, etc. All of our transportation ministries are called Outreach Ministry, and will reach out to all ages. This ministry includes ministry to families in financial distress as well. We provide transportation, financial assistance, Bible studies, and Children’s Christian Education through Sunday School, Youth fellowship, Puppet Ministry, Vacation Bible School, and many other activities throughout the year. Our goal is to strengthen the families of this community through the power of Jesus Christ in their lives.</p>

            <h3>MISSION CRESTWOOD</h3>

            <p>We have a partnership with Mission Crestwood, with facilities in the storage facility in Brownsboro, Ky. Our congregation has technology skills that can help spread the work of Mission Crestwood throughout the county, plus we are looking for new ways to help support their ongoing ministries to the economically struggling people of Oldham County.</p>

            <h3>HOPE CLINIC</h3>

            <p>The Hope Clinic is a ministry of the Oldham County Ministerial Association. It has successfully grown, and been recognized nationally as a model example of how the religious community can work with local government, local leaders and businesses, and state grants. It has grown so fast that it has outgrown the work of the Ministerial Association and is now an independent 501c-3. Our congregation has been involved at many different levels in the establishment of this local ministry. We support the clinic now with volunteer and financial support.</p>

            <h3>OLDHAM COUNTY MINISTERIAL ASSOCIATION</h3>

            <p>Pastor Earley has been President of the Ministerial Association since 2000. It is the largest organization of religious unity in the county and meets the second Wednesday of each month at Baptist Hospital NE from 12:30-2:00. For over two decades the Ministerial Association has sponsored the Good Samaritan Fund, which provides financial assistance to transient people and emergency needs. The Ministerial Association has been involved in many other activities through the years to better serve our county, and work to get the Christians of Oldham County to work together to spread the Gospel of Jesus Christ. Our congregation provides leadership and financial support for the Ministerial Associations work and events.</p>

            <h3>HABITAT FOR HUMANITY</h3>

            <p>This is a new mission of our congregation, being launched in 2015. The Oldham County Habitat has become much stronger since merging with the Louisville Habitat. There are a number of projects wrapping up, and new projects slated for 2015. We plan to provide financial assistance and volunteers for the Oldham County Habitat.</p>

            <h3>SOUTH OLDHAM MINISTERIAL ASSOCIATION CHRISTMAS BASKETS</h3>

            <p>We have been very involved with this ministry for many years. The number of families who receive baskets in the county has been going up in recent years. We have provided many volunteers to pack the boxes with food, financial assistance, volunteers to deliver the baskets, and caps for the children of the county (see CAPS For the World ministry written up in International Missions).</p>

            <h3>FIFTH SUNDAY OFFERINGS</h3>

            <p>There are five Sundays of the year that are Fifth Sundays. On these Sundays we make special offerings for local missions. Recently the main local missions we have supported are Christmas Baskets, Uspiritus Home for Children (Anchorage, KY).</p>

            <h3>MISSION PROJECTS OF THE PAST</h3>

            <p>Our congregation has sent money and mission teams all over the country. Some of the places we have served include: Washington DC Soup Kitchens, Chicago Projects, Dare to Care (Louisville), Gautier, Mississippi (Hurricane Katrina Relief), Arkansas, Wisconsin, and North Carolina.</p>
        </div>
    </section>
{% endblock %}
