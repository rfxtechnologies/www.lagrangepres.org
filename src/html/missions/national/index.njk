{% set canonical = domain + '/missions/national' %}
{% set pageName = "National Missions" %}

{% extends '_layouts/default.njk' %}

{% block main %}
    <section id="dark-content">
        <h1 class="section-title large">
            <p>National Missions</p>
        </h1>

        <div class="opening-quote">
            <p>Now that I, your Lord and Teacher, have washed your feet, you also should wash one another’s feet. I have set you an example that you should do as I have done for you.</p>

            <strong>John 13:14-15</strong>
        </div>

        <div class="gcontent">
            <p>We see missions as the life blood of the church, for Christ has called us to be servants one to another, as he modeled while washing His disciples’ feet. Our Regional missions include any work we do outside of Oldham County.</p>

            <h3>Red Bird Mission</h3>

            <p>This summer we are doing a mission project in Eastern Kentucky to help with a camp in Appalachia. The work will include small construction and repair work as it is needed at the camp. The volunteer mission program has a powerful impact on the individual Christian. Most Christians find that to go on a mission trip becomes one of the most meaningful experiences in their Christian life. The fellowship and friendships built with the rest of the team usually gives lifelong joy to the believer. When you serve someone else in need, you really do have the sense that you are doing it for Jesus Christ himself (Matthew 25:31-46).</p>

            <h3>Mission Arlington</h3>

            <p>Our youth fellowship is very active in local and regional missions. They are an integral part of our mission to Copperstone trailer park (see local missions). The youth have gone to Arlington, Texas for a number of years to do service projects, lead backyard Bible schools known as Rainbow Express, and shared the gospel with hundreds of kids. God has used our youth to help lead around 70 kids to Jesus Christ.</p>

            <h3>Wayside Christian Mission</h3>

            <p>We have sent teams to help at this downtown Louisville mission to the homeless. Those teams have served meals and done mission projects for the mission.</p>

            <h3>Mission Projects of the Past</h3>

            <p>Our congregation has sent money and mission teams all over the country. Some of the places we have served include: Washington DC Soup Kitchens, Chicago Projects, Dare to Care (Louisville), and Gautier, Mississippi (Hurricane Katrina Relief).</p>
        </div>
    </section>
{% endblock %}
