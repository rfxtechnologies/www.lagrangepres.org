'use strict';

import '@babel/polyfill';

import './modules/FormRecipient';

import { init as initBibleYear } from './modules/BibleYear';
import { init as initEvents } from './modules/Events';
import { init as initMobileNav } from './modules/MobileNav';
import { init as initMinistryContact } from './modules/MinistryContact';
import { init as initNavFlyouts } from './modules/NavFlyouts';
import { init as initRegReferral } from './modules/RegistrationReferral';
import { init as initContactForm } from './modules/ContactForm';
import { init as initRegisterForm } from './modules/RegisterForm';
import { init as initSlider } from './modules/SliderMain';
import { init as initVideoPlaylist } from './modules/VideoPlaylist';


initBibleYear();
initContactForm();
initEvents();
initMinistryContact();
initMobileNav();
initNavFlyouts();
initRegisterForm();
initRegReferral();
initSlider();
initVideoPlaylist();
