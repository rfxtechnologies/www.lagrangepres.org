
export const init = () => {
   const monthButtons = document.querySelectorAll('button[data-month]');
   const monthSections = document.querySelectorAll('.month');

   [...monthButtons].forEach(btn => {
      btn.addEventListener('click', ev => {
         ev.preventDefault();

         const monthVal = ev.target.getAttribute('data-month').toString().toLowerCase();

         monthSections.forEach(month => {
            const monthId = month.getAttribute('id').toString().toLowerCase();
            month.style.display = monthVal === monthId ? 'block' : 'none';
         });
      });
   });
};

export default init;
