import autobind from "class-autobind";
import { parse } from "parse-form";
import AWS from "aws-sdk/global";
import SNS from "aws-sdk/clients/sns.js";

const FORM_CLASS = "contact-form";
const FORM_PROCESSING_CLASS = "contact-form--submitting";

const NOTIFICATION_CLASS = "notification";
const NOTIFICATION_ERROR_CLASS = "notification--warning";

const AWS_REGION = "us-east-1";

const AWS_INFO = [
   {
      name: "General Contact",
      POOL_ID: "us-east-1:028e1eaf-1739-4f64-a52d-3e1d33f8fe6f",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGrangePres-SNS-SNSForms-1K6YQ0VCH5XZG"
   },
   {
      name: "Building and Grounds Ministry Team",
      POOL_ID: "us-east-1:0f66fe1f-6b9a-44ee-98f9-2cbeb9d419f6",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-BuildingGrounds-SNSForms-1J6B2FU4QJBS8"
   },
   {
      name: "Christian Education Ministry Team",
      POOL_ID: "us-east-1:bfa136c9-2bd6-4d7c-ab1f-58262807d848",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-ChristianEducation-SNSForms-1PYL4DK5UXE0J"
   },
   {
      name: "Pastor",
      POOL_ID: "us-east-1:fe497a07-5fde-470b-a062-54b7c5348674",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-Pastor-SNSForms-16WJ75LIAEZ4W"
   },
   {
      name: "Evangelism Ministry Team",
      POOL_ID: "us-east-1:d7002702-021a-4693-a2b5-ed250c7c7452",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-Evangelism-SNSForms-1NNLF6OSU177G"
   },
   {
      name: "Finance Ministry Team",
      POOL_ID: "us-east-1:2d181fe3-eabf-4b2c-8939-d9e002bf0c4d",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-Finance-SNSForms-Y0MFIA3FXIFP"
   },
   {
      name: "Missions Ministry Team",
      POOL_ID: "us-east-1:058cf6f2-6dcd-4d6d-8291-a44653a2676e",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-Missions-SNSForms-1MXAN4C7RTSLK"
   },
   {
      name: "Worship Ministry Team",
      POOL_ID: "us-east-1:eec73bda-480c-4029-9c06-4534b3ae98c2",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-Worship-SNSForms-FYJ2CT7G7IZA"
   },
   {
      name: "Small Groups Ministry Team",
      POOL_ID: "us-east-1:95268091-59be-44e3-8826-3d14fe245e29",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-SmallGroups-SNSForms-39BL7UP9JK44"
   },
   {
      name: "Youth Ministry Team",
      POOL_ID: "us-east-1:227aca8c-dffe-4253-83a9-03243818cd3d",
      ARN:
         "arn:aws:sns:us-east-1:474313850086:LaGP-YouthMinistry-SNSForms-VFQVCQRE8MFW"
   }
];

export class ContactForm {

   constructor(formElement) {
      autobind(this);

      const inputs = formElement.querySelectorAll(
         'input:not([type="submit"]), select, textarea'
      );

      this.props = {
         form: formElement,
         defaultSubject: "LaGrange Presbyterian - Contact Form Submission",
         inputs: inputs,
         inputRules: (() => {
            return [...inputs]
               .map((input, i) => {
                  return {
                     id: input.getAttribute("id"),
                     name: (() => {
                        const name = input.getAttribute("name") || "";

                        if (name.endsWith("[]")) {
                           return name.slice(0, -2);
                        }

                        return name;
                     })(),
                     tag: input.tagName.toLowerCase(),
                     type: input.getAttribute("type") || "text",
                     node: input,
                     required: input.hasAttribute("required") ? true : false,
                     maxLength: input.getAttribute("max-length"),
                     index: i
                  };
               })
               .filter(input => input.name !== "")
               .reduce((map, obj) => {
                  map[obj.name] = obj;
                  return map;
               }, {});
         })()
      };

      this.errors = [];

      this.setupForm();
   }

   getErrors() {
      return this.errors.slice(0);
   }

   setErrors(errors = []) {
      if (Array.isArray(errors)) {
         this.errors = errors;
      } else if (typeof errors === "string") {
         this.errors = [errors];
      }

      return this;
   }

   getNameLabel(key) {
      return (
         key
            .replace(/_-/g, " ")
            .charAt(0)
            .toUpperCase() + key.substr(1)
      );
   }

   buildEmailBody(data) {
      let msg = "\n\n";

      for (const prop in data) {
         const label = this.getNameLabel(prop);
         const val =
            typeof data[prop] === "string" ? data[prop] : data[prop].toString();

         if (prop === "subject") {
            continue;
         }

         msg += `${label}: ${val}\n\n`;
      }

      msg += "\n\n";

      return msg;
   }

   handleSuccess() {
      const { form } = this.props;
      const notify = document.createElement("div");
      const p = document.createElement("p");
      const text = document.createTextNode(`Thank you for your submission!`);

      notify.className = `${NOTIFICATION_CLASS}`;

      p.appendChild(text);
      notify.appendChild(p);

      form.innerHTML = "";
      form.appendChild(notify);

      return this;
   }

   handleError() {
      const { form } = this.props;
      const errors = this.getErrors();
      const notify = document.createElement("div");
      const ul = document.createElement("ul");

      notify.className = `${NOTIFICATION_CLASS} ${NOTIFICATION_ERROR_CLASS}`;

      errors.forEach(err => {
         const li = document.createElement("li");
         const text = document.createTextNode(err);
         li.appendChild(text);
         ul.appendChild(li);
      });

      notify.appendChild(ul);

      this.handleRemoveNotifications();

      form.insertBefore(notify, form.childNodes[0]);

      return this;
   }

   handleRemoveNotifications() {
      const form = this.props.form;
      const notifications = form.querySelectorAll(
         `.${NOTIFICATION_CLASS}, .${NOTIFICATION_ERROR_CLASS}`
      );

      [...notifications].forEach(node => {
         form.removeChild(node);
      });
   }

   cleanInputs() {
      [...this.props.inputs].forEach(input => {
         const val = input.value;
         const cleaned =
            typeof val === "string" ? val.trim().replace(/\s{2,}/g, " ") : val;
         input.value = cleaned;
      });

      return this;
   }

   validateEmail(value) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(value);
   }

   handleValidation(formData = {}) {
      const rules = Object.assign({}, this.props.inputRules);
      const keys = Object.keys(formData);

      const errors = keys
         .map(key => {
            const label = this.getNameLabel(key);

            if (!rules.hasOwnProperty(key)) {
               return `${label} should not be here...`;
            }

            const rule = rules[key];
            const data = formData[key];

            if (rule.required && (data === "" || data === null)) {
               return `${label} is required.`;
            }

            if (rule.type === "email" && !this.validateEmail(data)) {
               return `${label} is not a valid Email Address`;
            }

            if (rule.maxLength && data.length > rule.maxLength) {
               return `${label} should not exceed ${rule.maxLength} characters.`;
            }

            return null;
         })
         .filter(err => err !== null);

      return errors;
   }

   handleSendEmail(message, subject) {
      const self = this;
      const sub = subject || self.props.defaultSubject;
      const sns = new SNS();
      const formRecipient = this.determineFormRecipient();
      const arn = formRecipient.ARN;

      const params = {
         Message: message,
         Subject: sub,
         TopicArn: arn
      };

      sns.publish(params, (err, data) => {
         if (err) {
            self.setErrors([err.message]);
            self.handleError();
         } else {
            self.handleSuccess();
         }

         self.props.form.classList.remove(FORM_PROCESSING_CLASS);
      });
   }

   handleSubmit(ev) {
      ev.preventDefault();

      // moving aws setup to after submit has been hit so that recipient has already been chosen
      this.setupAwsConfig();

      this.cleanInputs();

      const formData = parse(this.props.form).body;
      const errors = this.handleValidation(formData);

      this.setErrors(errors);

      if (errors.length) {
         this.handleError();
         return false;
      }

      this.props.form.classList.add(FORM_PROCESSING_CLASS);

      const emailMessage = this.buildEmailBody(formData);
      const emailSubject = formData.hasOwnProperty("subject")
         ? formData.subject
         : this.props.defaultSubject;

      this.handleSendEmail(emailMessage, emailSubject);
   }

   determineFormRecipient() {
      const recipientField = document.getElementById('form-recipient');
      const recipientValue = recipientField && recipientField.value ? recipientField.value : 'General Contact';
      return AWS_INFO.find(obj => obj.name === recipientValue);
   }

   setupAwsConfig() {
      const recipient = this.determineFormRecipient();

      AWS.config.region = AWS_REGION;

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
         IdentityPoolId: recipient.POOL_ID
      });

      AWS.config.credentials.get(() => {
         //console.log("AWS credentials intialized");
      });
   }

   setupForm() {
      const { form } = this.props;
      form.addEventListener("submit", this.handleSubmit);
   }
}


export const init = () => {
   const formElements = document.querySelectorAll(`form.${FORM_CLASS}`);
   const forms = [...formElements].map(form => new ContactForm(form));
};

export default init;
