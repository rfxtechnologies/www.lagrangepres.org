
import moment from 'moment';

const API_KEY = 'AIzaSyAqt16_KeQnls7zWuYb7QpO-liGmy-eJPw';

const calendars = [
   'kui8r51s6s4ovi6nru15l7jfsk',
   'lhi8p3st5d2okinlvvvj2ok1q0',
   '6t5h9sfrc77ftqu8us459b5efg',
   '846qq44g6lk023hb7m585ucfk8',
   'jkbqr1a7v0bnd391e8sq11clco',
   '6btao7np9bgasedvdv7hrnl9pc',
   'ouksnhearocb04ci590g11ont4',
].map(id => {
   return [
      `https://www.googleapis.com/calendar/v3/calendars/`,
      `lagrangepres.org_${ id }`,
      `@group.calendar.google.com/events`,
      `?`,
      `orderby=startTime`,
      `&singleEvents=true`,
      `&maxResults=10`,
      `&timeMin=${  moment().hour(0).minutes(0).seconds(0).format(`YYYY-MM-DDTHH:mm:00ZZ`) }`,
      `&timeMax=${  moment().add(120, 'day').format(`YYYY-MM-DDTHH:mm:00ZZ`) }`,
      `&key=${ API_KEY }`,
   ].join('');
});

const getJSON = (url, callback) => {
   const xhr = new XMLHttpRequest();
   xhr.open('GET', url, true);
   xhr.responseType = 'json';

   xhr.onload = () => {
      const status = xhr.status;

      if (status === 200) {
         callback(xhr.response);
      }
      //else {
         //callback(status, xhr.response);
      //}
   };

   xhr.send();
};

const formatEvents = events => {
   return events.map(ev => {
      const dateStr = `
         <div class="date">
            <p>${ ev.monthAbbr }<br>${ ev.day }</p>
         </div>
      `;

      const timeStr = `
         <span class="time">
            ${ ev.startTime } - ${ ev.endTime }
         </span>
      `;

      const summary = `
         <span class="summary">
            ${ ev.summary || ev.location || 'Event' }
         </span>
      `;

      const anchor = ev.url ? `
         <a href="${ ev.url }" target="_blank">
            ${ summary }
         </a>
      ` : summary;

      return `
         <li>
            ${ dateStr } ${ timeStr } — ${ anchor }
         </li>
      `;
   })
   .join('');
};

const setupEvents = (results, containerNode) => {
   const events = results.sort((a, b) => {
      return new Date(a.start.dateTime || a.start.date).getTime() - new Date(b.start.dateTime || b.start.date).getTime();
   })
   .slice(0, 3)
   .map(item => {
      const startDateTime = (() => {
         if (item.start.hasOwnProperty('dateTime')) {
            return moment(item.start.dateTime);
         }

         return moment(item.start.date + ' 00:00:00');
      })();

      const endDateTime = (() => {
         if (item.end.hasOwnProperty('dateTime')) {
            return moment(item.end.dateTime);
         }

         return moment(item.end.date + ' 23:59:59');
      })();

      return {
         allDay: item.start.hasOwnProperty('date'),
         startDateTime: startDateTime.format('YYYY-MM-DD HH:mm:ss'),
         endDateTime: endDateTime.format('YYYY-MM-DD HH:mm:ss'),
         monthAbbr: startDateTime.format('MMM').toUpperCase(),
         day: startDateTime.format('D'),
         startTime: startDateTime.format('h:mmA'),
         endTime: endDateTime.format('h:mmA'),
         location: item.hasOwnProperty('location') ? item.location : null,
         summary: item.summary,
         url: item.htmlLink,
      };
   });

   containerNode.innerHTML = formatEvents(events);
};

export const init = () => {
   const containerNode = document.getElementById('lpc-events');

   if (!containerNode) {
      return;
   }

   const apiCalls = [];
   let results = [];

   [...calendars].forEach(url => {
      const p = new Promise((resolve, reject) => {
         getJSON(url, data => {
            const items = data.items.filter(item => {
               return item.hasOwnProperty('status') && item.status !== 'cancelled';
            });

            results = results.concat(items);
            resolve();
         });
      });

      apiCalls.push(p);
   });

   Promise.all(apiCalls)
   .then(() => {
      setupEvents(results, containerNode);
   });
};

export default init;
