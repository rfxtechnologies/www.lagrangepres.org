//Check for localstorage for contact form

(function() {

    if (localStorage.getItem('specificContact')) {
        //console.log('ls:'+localStorage.getItem('specificContact'));
        if (document.getElementById('form-recipient')) {
            document.getElementById('form-recipient').value = localStorage.getItem('specificContact');
            localStorage.removeItem("specificContact");
        }
    }

})();
