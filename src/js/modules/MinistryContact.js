
export const init = () => {
   const contactNode = document.getElementById('contact-this-person');
   const team = contactNode ? contactNode.getAttribute('data-team') : null;

   if (!contactNode || !team) {
      return;
   }

   contactNode.addEventListener('click', ev => {
      ev.preventDefault();
      localStorage.setItem("specificContact", team);
      window.location.href = '/contact/';
   });
};

export default init;
