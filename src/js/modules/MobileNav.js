
export const init = () => {
   const mobileNav = document.getElementById('mobile-nav');
   const topNav = document.getElementById('top-nav');
   const pageContent = document.getElementById('page-content');

   /* Handle Open Mobile Nav */
   mobileNav.addEventListener('click', ev => {
      topNav.setAttribute('class', 'active');
      pageContent.setAttribute('class', 'active');
      ev.preventDefault();
      ev.stopPropagation();
   });

   /* Handle Keep Mobile Nav Open */
   topNav.addEventListener('click', ev => {
      if (ev.target.getAttribute('id') !== 'close-mobile-nav') {
         ev.stopPropagation();
      }
   });

   /* Handle Close Mobile Nav */
   ['masthead-container', 'dark-content', 'page-content', 'close-mobile-nav']
   .map(id => document.getElementById(id))
   .filter(node => typeof node !== undefined)
   .forEach(node => {
      node.addEventListener('click', ev => {
         topNav.setAttribute('class', '');
         pageContent.setAttribute('class', '');
      });
   });
};

export default init;
