
import { hasClass } from '../utils/class-utils';

export const init = () => {
   const pageContent = document.getElementById('page-content');

   if (!pageContent) {
      return false;
   }

   let currentOpen = null;

   const allFlyouts = document.querySelectorAll('.flyout-content');
   const allFlyoutClicks = document.querySelectorAll('.flyout-click');
   const flyoutNode = document.getElementById('flyout');

   function clearFlyouts() {
      [...allFlyouts].forEach(el => el.setAttribute('class', 'flyout-content'));
      [...allFlyoutClicks].forEach(el => el.setAttribute('class', 'flyout-click'));
   }

   pageContent.addEventListener('click', ev => {
      const target = ev.target;
      const targetParent = ev.target.parentNode;

      let clickedFlyId;

      if (hasClass(targetParent, 'flyout-click')) {
         flyoutNode.setAttribute('class', '');
         clickedFlyId = targetParent.dataset.fly;
      }
      else if (hasClass(targetParent, 'top-layer')) {
         flyoutNode.setAttribute('class', '');
         clickedFlyId = target.dataset.fly;
      }

      if (clickedFlyId) {
         const activeFlyNode = document.getElementById(clickedFlyId);
         const flyoutBarNode = document.querySelector(`[data-fly="${ clickedFlyId }"]`);

          if (currentOpen == clickedFlyId) {
             currentOpen = null;
             flyoutNode.setAttribute('class', 'none');
             flyoutBarNode.setAttribute('class', 'flyout-click');
          }
          else {
             currentOpen = clickedFlyId;
          }

          clearFlyouts();

          //mobile check, handles differently
         const windowWidth = window.innerWidth;

         if (windowWidth <= 599) {
             let offsetTop = 0;

             //each nav item is 38px tall;
             let navItemHeight = 38;

             if (clickedFlyId === 'leftfly') {
                 offsetTop = navItemHeight * 2;
             }
             else if (clickedFlyId == 'midfly') {
                 offsetTop = navItemHeight;
             }

             flyoutNode.setAttribute('style', `top: -${ offsetTop }px`);
         }

         flyoutBarNode.setAttribute('class', 'flyout-click active');
         activeFlyNode.setAttribute('class', 'flyout-content active');
      }
   });





};

export default init;
