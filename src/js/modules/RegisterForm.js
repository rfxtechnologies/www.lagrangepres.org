import autobind from "class-autobind";
import { parse } from "parse-form";
import AWS from "aws-sdk/global";
import SNS from "aws-sdk/clients/sns.js";

const FORM_CLASS = "register-form";
const FORM_PROCESSING_CLASS = "contact-form--submitting";

const NOTIFICATION_CLASS = "notification";
const NOTIFICATION_ERROR_CLASS = "notification--warning";

const AWS_REGION = "us-east-1";
const AWS_COGNITO_IDENTITY_POOL_ID =
   "us-east-1:75c3312e-1f34-4983-ae04-450f079f6b66";
const AWS_TOPIC_ARN =
   "arn:aws:sns:us-east-1:474313850086:HeartoftheFatherRegistration-SNSForms-1LEHMHADD69OE";

export class RegisterForm {
   constructor(formElement) {
      autobind(this);

      const inputs = formElement.querySelectorAll(
         'input:not([type="submit"]), select, textarea'
      );

      this.props = {
         form: formElement,
         defaultSubject:
            "LaGrange Pres - Heart of the Father Registration Submission",
         inputs: inputs,
         inputRules: (() => {
            return [...inputs]
               .map((input, i) => {
                  return {
                     id: input.getAttribute("id"),
                     name: (() => {
                        const name = input.getAttribute("name") || "";

                        if (name.endsWith("[]")) {
                           return name.slice(0, -2);
                        }

                        return name;
                     })(),
                     tag: input.tagName.toLowerCase(),
                     type: input.getAttribute("type") || "text",
                     node: input,
                     required: input.hasAttribute("required") ? true : false,
                     maxLength: input.getAttribute("max-length"),
                     index: i
                  };
               })
               .filter(input => input.name !== "")
               .reduce((map, obj) => {
                  map[obj.name] = obj;
                  return map;
               }, {});
         })()
      };

      this.errors = [];

      this.setupForm();
   }

   getErrors() {
      return this.errors.slice(0);
   }

   setErrors(errors = []) {
      if (Array.isArray(errors)) {
         this.errors = errors;
      } else if (typeof errors === "string") {
         this.errors = [errors];
      }

      return this;
   }

   getNameLabel(key) {
      return (
         key
            .replace(/_-/g, " ")
            .charAt(0)
            .toUpperCase() + key.substr(1)
      );
   }

   buildEmailBody(data) {
      let msg = "\n\n";

      for (const prop in data) {
         const label = this.getNameLabel(prop);
         const val =
            typeof data[prop] === "string" ? data[prop] : data[prop].toString();

         if (prop === "subject") {
            continue;
         }

         msg += `${label}: ${val}\n\n`;
      }

      msg += "\n\n";

      return msg;
   }

   handleSuccess() {
      const { form } = this.props;
      const notify = document.createElement("div");
      const p = document.createElement("p");
      const text = document.createTextNode(`Thank you for your submission!`);

      notify.className = `${NOTIFICATION_CLASS}`;

      p.appendChild(text);
      notify.appendChild(p);

      form.innerHTML = "";
      form.appendChild(notify);

      return this;
   }

   handleError() {
      const { form } = this.props;
      const errors = this.getErrors();
      const notify = document.createElement("div");
      const ul = document.createElement("ul");

      notify.className = `${NOTIFICATION_CLASS} ${NOTIFICATION_ERROR_CLASS}`;

      errors.forEach(err => {
         const li = document.createElement("li");
         const text = document.createTextNode(err);
         li.appendChild(text);
         ul.appendChild(li);
      });

      notify.appendChild(ul);

      this.handleRemoveNotifications();

      form.insertBefore(notify, form.childNodes[0]);

      return this;
   }

   handleRemoveNotifications() {
      const form = this.props.form;
      const notifications = form.querySelectorAll(
         `.${NOTIFICATION_CLASS}, .${NOTIFICATION_ERROR_CLASS}`
      );

      [...notifications].forEach(node => {
         form.removeChild(node);
      });
   }

   cleanInputs() {
      [...this.props.inputs].forEach(input => {
         const val = input.value;
         const cleaned =
            typeof val === "string" ? val.trim().replace(/\s{2,}/g, " ") : val;
         input.value = cleaned;
      });

      return this;
   }

   validateEmail(value) {
      const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(value);
   }

   handleValidation(formData = {}) {
      const rules = Object.assign({}, this.props.inputRules);
      const keys = Object.keys(formData);

      const errors = keys
         .map(key => {
            const label = this.getNameLabel(key);

            if (!rules.hasOwnProperty(key)) {
               return `${label} should not be here...`;
            }

            const rule = rules[key];
            const data = formData[key];

            if (rule.required && (data === "" || data === null)) {
               return `${label} is required.`;
            }

            if (rule.type === "email" && !this.validateEmail(data)) {
               return `${label} is not a valid Email Address`;
            }

            if (rule.maxLength && data.length > rule.maxLength) {
               return `${label} should not exceed ${rule.maxLength} characters.`;
            }

            return null;
         })
         .filter(err => err !== null);

      return errors;
   }

   handleSendEmail(message, subject) {
      const self = this;
      const sub = subject || self.props.defaultSubject;
      const sns = new SNS();

      const params = {
         Message: message,
         Subject: sub,
         TopicArn: AWS_TOPIC_ARN
      };

      sns.publish(params, (err, data) => {
         if (err) {
            self.setErrors([err.message]);
            self.handleError();
         } else {
            self.handleSuccess();
         }

         self.props.form.classList.remove(FORM_PROCESSING_CLASS);
      });
   }

   handleSubmit(ev) {
    console.log("submitting");
      ev.preventDefault();
    
      this.cleanInputs();

      const formData = parse(this.props.form).body;
      const errors = this.handleValidation(formData);

      this.setErrors(errors);

      if (errors.length) {
         this.handleError();
         return false;
      }

      this.props.form.classList.add(FORM_PROCESSING_CLASS);

      const emailMessage = this.buildEmailBody(formData);
      const emailSubject = formData.hasOwnProperty("subject")
         ? formData.subject
         : this.props.defaultSubject;

      this.handleSendEmail(emailMessage, emailSubject);
   }

   setupForm() {
      const { form } = this.props;
      form.addEventListener("submit", this.handleSubmit);
   }
}

const setupAwsConfig = () => {
   AWS.config.region = AWS_REGION;

   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: AWS_COGNITO_IDENTITY_POOL_ID
   });

   AWS.config.credentials.get(() => {
      //console.log("AWS credentials intialized");
   });
};

export const init = () => {
   const formElements = document.querySelectorAll(`.${FORM_CLASS}`);
   const forms = [...formElements].map(form => new RegisterForm(form));

   if (forms.length) {
      setupAwsConfig();
   }
};

export default init;
