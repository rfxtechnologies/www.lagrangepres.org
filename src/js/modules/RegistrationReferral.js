
const FORM_CLASS = "register-form",
      REFERRAL_CLASS = "referral-box",
      REFERRAL_OTHER_CLASS = "referral-other";

const setup = () => {
   const refBox = document.querySelector(`.${ REFERRAL_CLASS }`);
   const otherBox = document.querySelector(`.${ REFERRAL_OTHER_CLASS }`);

   if (!refBox || !otherBox) {
      return;
   }

   refBox.addEventListener("change", () => {
      if (refBox.value === "other") {
         otherBox.style.display = "block";
      }
   });
};

export const init = () => {
   const form = document.querySelector(`.${FORM_CLASS}`);

   if (form) {
      setup();
   }
};

export default init;
