
import { hasClass, addClass, removeClass } from '../utils/class-utils';

let autoPlay = null;

const switchSlide = (activeIndex, sliderNodes, dotsContainerNode) => {
   const dotNodes = dotsContainerNode ? [...dotsContainerNode.children] : [];

   [...sliderNodes].forEach((slide, i) => {
      const isActive = activeIndex === i;

      if (isActive) {
          addClass(slide, 'active');
      }
      else {
         removeClass(slide, 'active');
      }

      if (dotNodes.hasOwnProperty(i)) {
         if (isActive) {
             addClass(dotNodes[i], 'active');
         }
         else {
            removeClass(dotNodes[i], 'active');
         }
      }

   });
};

const runInterval = () => {
   autoPlay = setInterval(() => {
      const slides = document.querySelectorAll('#slider > div');
      const sliderDotsContainer = document.getElementById('slider-dots');

      let nextIndex = 0;

      const activeIndex = (() => {
         let index = 0;

         [...slides].forEach((slide, i) => {
            if (hasClass(slide, 'active')) {
               index = i;
            }
         });

         return index;
      })();

      nextIndex = activeIndex + 1;

      if (nextIndex >= slides.length) {
         nextIndex = 0;
      }

      switchSlide(nextIndex, slides, sliderDotsContainer);
   }, 7000);
};

const setupDots = (containerNode, sliderNodes) => {
   [...sliderNodes].forEach((slide, i) => {
      const dotNode = document.createElement('li');

      if (i === 0) {
         dotNode.setAttribute('class', 'active');
      }

      dotNode.addEventListener('click', ev => {
         switchSlide(i, sliderNodes, containerNode);
         clearInterval(autoPlay);
         runInterval();
      });

      containerNode.appendChild(dotNode);
   });
};

export const init = () => {
   const slider = document.getElementById('slider');

   if (!slider) {
      return;
   }

   const slides = document.querySelectorAll('#slider > div');
   const sliderDotsContainer = document.getElementById('slider-dots');

   /* Create Dots */
   if (sliderDotsContainer) {
      setupDots(sliderDotsContainer, slides);
      runInterval();
   }
};

export default init;
