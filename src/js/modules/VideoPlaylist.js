import 'element-closest';
import { addClass, removeClass } from '../utils/class-utils';


const handleMenuClick = (ev, frame, menu) => {
   ev.stopPropagation();
   ev.preventDefault();

   const target = ev.target;

   if (target.tagName.toLowerCase() !== 'a') {
      return false;
   }

   const vidId = target.getAttribute('data-id');
   const menuItems = menu ? menu.querySelectorAll('.video-menu__item') : [];

   frame.src = `https://www.youtube-nocookie.com/embed/${ vidId }?autoplay=1`;

   [...menuItems].forEach(node => {
      removeClass(node, 'video-menu__item--selected');
   });

   addClass(target.parentNode, 'video-menu__item--selected');
};

const setupPlaylist = node => {
   const videoFrame = node.querySelector('.video__frame');
   const menu = node.querySelector('.video-menu');
   const menuItems = menu ? menu.querySelectorAll('.video-menu__btn') : [];

   if (!videoFrame || !menu) {
      return;
   }

   menu.addEventListener('click', ev => {
      handleMenuClick(ev, videoFrame, menu);
   });
};

export const init = () => {
   const playlists = document.querySelectorAll('.vid-playlist');

   [...playlists].forEach(node => setupPlaylist(node));
};

export default init;
