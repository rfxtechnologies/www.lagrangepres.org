/**
 * LaGrange Footer upcoming events
 * based on example with https://github.com/MilanKacurak/FormatGoogleCalendar
 * Heavily modified with multiple calendar support and other improvements
 */


$(function(){

    //Setup deParty Events on homepage
    lpcEvents.init({
        upcoming: true,
        sameDayTimes: true,
        pastTopN: -1,
        upcomingTopN: 3,
        format: ['*date*', ' &mdash; ', '*summary*']
    });


});

var lpcEvents = (function() {

    'use strict';

    var config;

    //Gets JSON from Google Calendar and transfroms it into html list items and appends it to past or upcoming events list
    function init(settings) {
        var result = [];
        var apiKey = 'AIzaSyAqt16_KeQnls7zWuYb7QpO-liGmy-eJPw';

        config = settings;

        var calendars = [
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_kui8r51s6s4ovi6nru15l7jfsk%40group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_lhi8p3st5d2okinlvvvj2ok1q0%40group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_6t5h9sfrc77ftqu8us459b5efg%40group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,

            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_846qq44g6lk023hb7m585ucfk8%40group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_jkbqr1a7v0bnd391e8sq11clco@group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_6btao7np9bgasedvdv7hrnl9pc@group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey,
            'https://www.googleapis.com/calendar/v3/calendars/lagrangepres.org_ouksnhearocb04ci590g11ont4@group.calendar.google.com/events?orderby=startTime&singleEvents=true&maxResults=2500&key='+apiKey
        ];

        var totalCalendars = calendars.length;
        var successCounter = 0;


        $.each(calendars, function (x, value) {

            //Get JSON, parse it, transform into list items and append it to past or upcoming events list
            $.getJSON(calendars[x], function(data) {
                // Remove any cancelled events
                data.items.forEach(function (item) {
                    if (item && item.hasOwnProperty('status') && item.status !== 'cancelled') {
                        result.push(item);
                    }
                });
            }).done(function() {
                successCounter++;
                //Successfully retrieved all the calendars we wanted, ready to setup the list
                if (successCounter == totalCalendars) {
                    setupEvents();
                }
            });



        });


        function setupEvents() {

            //Sort the final, completed list
            result.sort(comp).reverse();

            var upcomingCounter = 0,
                upcomingResult = [],
                upcomingResultTemp = [],
                $upcomingElem = $('#lpc-events'),
                endDateAfterNormalizing,
                i;


            if (settings.upcomingTopN === -1) {
                settings.upcomingTopN = result.length;
            }

            if (settings.upcoming === false) {
                settings.upcomingTopN = 0;
            }

            for (i in result) {

                //All day events get a date property, normal ones get dateTime. Properly sorting them gets weird with the
                //added time at the end, so we normalize them to sort first.

                if (!result[i].end.date && result[i].end.dateTime) {
                    endDateAfterNormalizing = result[i].end.dateTime.substr(0,10);
                } else {
                    endDateAfterNormalizing = result[i].end.date;
                }


                //After normalizing, we're left with a date with no time. This is of course will cause no events on that day to show.
                //Here, we add a Time equal to 11:59PM to allow it show all events for the day. Small consequence will be that it will show events that have ended
                //for the day itself, but I don't believe this is a problem.
                endDateAfterNormalizing = endDateAfterNormalizing + 'T23:59:00';


                //If not a past event, push to the temp array
                if (!isPast(endDateAfterNormalizing)) {
                    upcomingResultTemp.push(result[i]);
                }

            }

            upcomingResultTemp.reverse();

            for (i in upcomingResultTemp) {
                if (upcomingCounter < settings.upcomingTopN) {
                    upcomingResult.push(upcomingResultTemp[i]);
                    upcomingCounter++;
                }
            }

            for (i in upcomingResult) {

                //Hide loading element once we have the events
                if (i == 0) {
                    $upcomingElem.find('.loading-events').hide();
                }

                $upcomingElem.append(transformationList(upcomingResult[i], settings.format));
            }
        }

    };

    //Compare dates
    function comp(a, b) {
        return new Date(a.start.dateTime || a.start.date).getTime() - new Date(b.start.dateTime || b.start.date).getTime();
    };

    //Overwrites defaultSettings values with overrideSettings and adds overrideSettings if non existent in defaultSettings
    function mergeOptions(defaultSettings, overrideSettings){
        var newObject = {},
            i;
        for (i in defaultSettings) {
            newObject[i] = defaultSettings[i];
        }
        for (i in overrideSettings) {
            newObject[i] = overrideSettings[i];
        }
        return newObject;
    };

    //Get all necessary data (dates, location, summary, description) and creates a list item
    function transformationList(result, format) {
        var dateStart = getDateInfo(result.start.dateTime || result.start.date),
            dateEnd = getDateInfo(result.end.dateTime || result.end.date),
            dateFormatted = getFormattedDate(dateStart, dateEnd),
            output = '<li>',
            url = result.htmlLink || '',
            summary = result.summary || '',
            description = result.description || '',
            location = result.location || '',
            i;


        for (i = 0; i < format.length; i++) {

            format[i] = format[i].toString();

            if (format[i] === '*summary*') {

                if (url != '') {
                    output = output.concat('<a href="'+url+'" target="_blank">');
                }
                output = output.concat('<span class="summary">' + summary + '</span>');

                if (url != '') {
                    output = output.concat('</a>');
                }
            } else if (format[i] === '*date*') {
                output = output.concat('<div class="date"><p>' + dateFormatted + '</p></div>');
                var formattedTime = '';
                formattedTime = ' '+ getFormattedTime(dateStart) + ' - ' + getFormattedTime(dateEnd);
                output = output.concat('<span class="time">'+formattedTime+'</span>');
            } else {
                if ((format[i + 1] === '*location*' && location !== '') ||
                    (format[i + 1] === '*summary*' && summary !== '')) {
                    output = output.concat(format[i]);
                }
            }
        }

        return output + '</li>';
    };

    //Check if date is later then now
    function isPast(date) {
        var compareDate = new Date(date),
            now = new Date();


        if (now.getTime() > compareDate.getTime()) {
            return true;
        }

        return false;
    };

    //Get temp array with information about day in followin format: [day number, month number, year]
    function getDateInfo(date) {
        date = new Date(date);
        return [date.getDate(), date.getMonth(), date.getFullYear(), date.getHours(), date.getMinutes()];
    };

    //Get month name according to index
    function getMonthName(month) {
        var monthNames = [
            'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'
        ];

        return monthNames[month];
    };

    //Transformations for formatting date into human readable format
    function formatDateSameDay(dateStart, dateEnd) {


        //month day, time-time
        return getMonthName(dateStart[1]) + '<br />' + dateStart[0];
    };

    function formatDateDifferentDay(dateStart, dateEnd) {
        //month day-day
        return getMonthName(dateStart[1]) + ' ' + dateStart[0] + '-' + dateEnd[0];
    };

    function formatDateDifferentMonth(dateStart, dateEnd) {
        //month day - month day
        return getMonthName(dateStart[1]) + ' ' + dateStart[0] + '-' + getMonthName(dateEnd[1]) + ' ' + dateEnd[0];
    };

    function formatDateDifferentYear(dateStart, dateEnd) {
        //month day, year - month day
        return getMonthName(dateStart[1]) + ' ' + dateStart[0] + ', ' + dateStart[2] + '-' + getMonthName(dateEnd[1]) + ' ' + dateEnd[0];
    };

    //Check differences between dates and format them
    function getFormattedDate(dateStart, dateEnd) {
        var formattedDate = '';

        if (dateStart[0] === dateEnd[0]) {
            if (dateStart[1] === dateEnd[1]) {
                if (dateStart[2] === dateEnd[2]) {
                    //month day, year
                    formattedDate = formatDateSameDay(dateStart, dateEnd);
                } else {
                    //month day, year - month day, year
                    formattedDate = formatDateDifferentYear(dateStart, dateEnd);
                }
            } else {
                if (dateStart[2] === dateEnd[2]) {
                    //month day - month day, year
                    formattedDate = formatDateDifferentMonth(dateStart, dateEnd);
                } else {
                    //month day, year - month day, year
                    formattedDate = formatDateDifferentYear(dateStart, dateEnd);
                }
            }
        } else {
            if (dateStart[1] === dateEnd[1]) {
                if (dateStart[2] === dateEnd[2]) {
                    //month day-day, year
                    formattedDate = formatDateDifferentDay(dateStart, dateEnd);
                } else {
                    //month day, year - month day, year
                    formattedDate = formatDateDifferentYear(dateStart, dateEnd);
                }
            } else {
                if (dateStart[2] === dateEnd[2]) {
                    //month day - month day, year
                    formattedDate = formatDateDifferentMonth(dateStart, dateEnd);
                } else {
                    //month day, year - month day, year
                    formattedDate = formatDateDifferentYear(dateStart, dateEnd);
                }
            }
        }

        return formattedDate;
    };

    function getFormattedTime(date) {
        var formattedTime = '',
            period = 'AM',
            hour = date[3],
            minute = date[4];

        // Handle afternoon.
        if (hour >= 12) {
            period = 'PM';

            if (hour >= 13) {
                hour -= 12;
            }
        }

        // Handle midnight.
        if (hour === 0) {
            hour = 12;
        }

        // Ensure 2-digit minute value.
        minute = (minute < 10 ? '0' : '') + minute;

        // Format time.
        formattedTime = hour + ':' + minute + period;
        return formattedTime;
    };

    return {
        init: function (settingsOverride) {
            var settings = {
                upcoming: true,
                sameDayTimes: true,
                pastTopN: -1,
                upcomingTopN: -1,
                itemsTagName: 'li',
                format: ['*date*', '*summary*', ' &mdash; ', '*description*', ' in ', '*location*']
            };

            settings = mergeOptions(settings, settingsOverride);

            init(settings);
        }
    };
})();
