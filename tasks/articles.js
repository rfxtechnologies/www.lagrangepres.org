/**
 * PART 3 OF 3
 *     - Writes tmp/pastor/articles/<slug>/index.njk
 */

import fs from "fs";
import { getJsonData } from "./util/jsonData";
import glob from "glob";
import { processFile as parseToJSON } from "md-yaml-json";
import moment from "moment";
import path from "path";

export function articles(src = [], onComplete = (f) => f) {
   console.log("Building Articles Pages... (this might take a minute)");

   return new Promise((resolve, reject) => {
      const tmpFolder = "./tmp";
      let jsonData = getJsonData();

      const articleFiles = glob.sync("src/html/_pastor-articles/**/*.md", {});

      articleFiles.forEach((file) => {
         const articleData = parseToJSON(path.resolve(__dirname, `../${file}`));

         const articleEntry = {
            title: articleData.meta.title,
            date: articleData.meta.date,
            dateStr: moment(articleData.meta.date, "MM-DD-YYYY").format(
               "MMMM D, YYYY"
            ),
            slug: articleData.meta.slug,
            summary: articleData.meta.summary,
            content: articleData.html,
         };

         const dirName = articleEntry.slug
            ? articleEntry.slug
            : file.split("/").pop().replace(".md", "");

         const articlePath = `/pastor/articles/${dirName}`;

         if (!fs.existsSync(tmpFolder + articlePath)) {
            fs.mkdirSync(tmpFolder + articlePath);
         }

         jsonData.articleEntry = articleEntry;
         jsonData.path = articlePath;
         jsonData.canonical = `${jsonData.domain}${jsonData.path}`;

         const articleTplStr = `{% set blogArticle = ${JSON.stringify(
            articleEntry, null, 4
         )} %} {% extends '_layouts/articles.njk' %}`;

         fs.writeFileSync(
            `${tmpFolder}${articlePath}/index.njk`,
            articleTplStr
         );
      });

      resolve();
   }).catch((error) => console.log(error));
}
