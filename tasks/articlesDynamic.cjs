const fs = require('fs');
const { getJsonData } = require('./util/jsonData.cjs');

function articlesDynamic() {
   console.log("Building Articles Dynamic Pages");

   return new Promise((resolve, reject) => {
      const tmpFolder = "./tmp";

      let jsonData = getJsonData();

      let currentPage = jsonData.article.totalPages;

      while (currentPage > 0) {
         const pagePath = `/pastor/page/${currentPage}`;

         if (!fs.existsSync(tmpFolder + pagePath)) {
            fs.mkdirSync(tmpFolder + pagePath);
         }
         const pageTplString = `{% set blogPage = ${currentPage} %} {% extends '_layouts/articles-index.njk' %}`;

         fs.writeFileSync(`${tmpFolder}${pagePath}/index.njk`, pageTplString);

         currentPage--;
      }

      let datesObj = jsonData.article.yearsAndMonths;

      for (const key of Object.keys(datesObj)) {
         const year = key;
         const months = datesObj[key];
         const yearPath = `/pastor/archive/${year}`;

         if (!fs.existsSync(tmpFolder + yearPath)) {
            fs.mkdirSync(tmpFolder + yearPath);
         }

         const yearTplString = `{% set blogYear = "${year}" %} {% extends '_layouts/articles-year.njk' %}`;

         fs.writeFileSync(`${tmpFolder}${yearPath}/index.njk`, yearTplString);

         months.forEach((month) => {
            const monthTplString = `{% set blogYear = "${year}" %}{% set blogMonth = "${month}" %} {% extends '_layouts/articles-month.njk' %}`;
            const monthPath = `/pastor/archive/${year}/${month}`;

            if (!fs.existsSync(tmpFolder + monthPath)) {
               fs.mkdirSync(tmpFolder + monthPath);
            }

            fs.writeFileSync(
               `${tmpFolder}${monthPath}/index.njk`,
               monthTplString
            );
         });
      }

      resolve();
   });
}

module.exports = Object.assign(articlesDynamic, {
   articlesDynamic,
});
