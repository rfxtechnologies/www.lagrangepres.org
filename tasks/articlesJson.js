/**
 * PART 1 OF 3
 * Writes:
 *     - src/html/_data/articles.json
 */

import frontMatter from "gulp-front-matter";
import gulp from "gulp";
import data from "gulp-data";
import pluck from "gulp-pluck";
import moment from "moment";
import colors from "colors/safe";

const distFolder = "./src/html/_data";
const allMonths = [
   "January",
   "February",
   "March",
   "April",
   "May",
   "June",
   "July",
   "August",
   "September",
   "October",
   "November",
   "December",
];

let years = [];

export function articlesJson() {
   console.log("Building Articles JSON Data... (this might take a minute)");
   return (
      gulp
         .src(["src/html/_pastor-articles/**/*.md"])
         .pipe(frontMatter())
         .pipe(pluck("frontMatter", "articles.json"))
         .pipe(
            data((file) => {
               let dateObj = {};

               const articlesArr = file.frontMatter
                  .map((obj) => {
                     const year = moment(obj.date, "MM-DD-YYYY").format("YYYY");
                     const month = moment(obj.date, "MM-DD-YYYY").format(
                        "MMMM"
                     );

                     if (!Array.isArray(dateObj[year])) {
                        dateObj[year] = [];
                     }
                     if (!dateObj[year].includes(month)) {
                        dateObj[year].push(month);
                     }

                     dateObj[year].sort(
                        (a, b) => allMonths.indexOf(a) - allMonths.indexOf(b)
                     );

                     years.push(year);

                     return Object.assign({}, obj, {
                        date: moment(obj.date, "MM-DD-YYYY").format(
                           "YYYY MM DD"
                        ),
                        dateStr: moment(obj.date, "MM-DD-YYYY").format(
                           "MMM D, YYYY"
                        ),
                        year: year,
                        month: month,
                     });
                  })
                  .sort((a, b) => (a.date > b.date ? 1 : -1))
                  .reverse();

               const articlesPerPage = 7;



               const articleData = {
                  article: {
                     articles: articlesArr,
                     yearsAndMonths: dateObj,
                     years: [...new Set(years)].filter(yr => yr).sort(),
                     itemsPerPage: articlesPerPage,
                     totalPages: Math.ceil(
                        articlesArr.length / articlesPerPage
                     ),
                  },
               };

               file.contents = new Buffer.from(JSON.stringify(articleData, null, 4));
            })
         )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            this.emit("end");
         })
         /* Output html files  */
         .pipe(gulp.dest(distFolder))
   );
}
