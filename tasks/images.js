/*
   Reades css files in src/css and moves them to the /public/assets/css folder.

   PostCSS is enabled allowing you to use many future CSS techniques not yet available
   in the browser that will be polyfilled in your compiled code... as well as some Sass-like helpers.

   Also allows the ability to import files from node_module libraries as well as your custom
   modularized css files
*/
import fs from 'fs';
import gulp from 'gulp';
import gulpIf from 'gulp-if';
import debug from 'gulp-debug';
import colors from 'colors/safe';
//import filter from 'gulp-filter';

import imagemin from 'gulp-imagemin';


/* determine if in production or dev mode */
const devMode = process.env.NODE_ENV !== 'production';


export function gulpImages() {
   /* Scan css files in the src/images folder */
   return gulp.src([
      'src/images/**/*'
   ])
   // .pipe(imagemin([
   //    imagemin.gifsicle({ interlaced: true }),
   //    imagemin.jpegtran({ progressive: true }),
   //    imagemin.optipng({ optimizationLevel: 5 }),
   //    imagemin.svgo({
   //     plugins: [
	// 		{ removeViewBox: true },
	// 		{ cleanupIDs: false }
	//     ]
	//   })
   // ]))
   .on('error', function(err) {
      console.log(colors.red(err.toString()));
      this.emit('end');
   })

    /* Output css file(s) */
	.pipe(gulp.dest('public/images'));
}
