/*jshint esversion: 11 */
/*jshint -W003 */

/*
   Gulp entry file with your main commands
*/

import fs from "fs-extra";
import del from "del";
import gulp from "gulp";
import { watch as gulpWatch } from "gulp";
import { scripts } from "./webpack";
import { server, serverReload } from "./server";
import { nunjucks as nunjucksRender } from "./nunjucks";
import { gulpCss } from "./css";
import { gulpImages } from "./images";
import { gulpSass } from "./sass";
import { siteMap } from "./siteMap";
import { articles } from "./articles";
import { articlesJson } from "./articlesJson";
import { articlesDynamic } from "./articlesDynamic";

function copyStaticFiles(cb) {
   fs.copySync("./src/static", "./public");
   cb?.();
}

function createTmpFolders(cb) {
   const tmpFolder = "./tmp";
   del.sync(tmpFolder, {
      force: true,
   });
   if (!fs.existsSync(tmpFolder)) {
      fs.mkdirSync(tmpFolder, { recursive: true });
   }
   const tmpFolders = [
      "/pastor",
      "/pastor/articles",
      "/pastor/page",
      "/pastor/archive",
      "/images"
   ];
   tmpFolders.forEach((path) =>
      fs.mkdirSync(tmpFolder + path, { recursive: true })
   );
   cb?.();
}

const htmlHandler = (path, stats) => {
   if (path.startsWith("src\\html\\_")) {
      console.log("Rebuilding site... this may take a minute.");
      nunjucksRender([
         "src/html/**/*",
         "!src/html/_*/",
         "!src/html/_*/**/*",
      ]).on("end", function (err) {
         serverReload();
      });
   } else {
      const files = Array.isArray(path) ? path : [path];
      nunjucksRender(files, serverReload);
   }
};

const articlesHandler = (path, stats) => {
   articlesJson().on("end", function () {
      articles();
   });
};

const sassHandler = (path, stats) => {
   gulpSass().on("end", function (err) {
      serverReload();
   });
};

const jsHandler = (path, stats) => {
   scripts().then(() => {
      serverReload();
   });
};

function resetDistFolder(cb) {
   const distFolder = "./public";
   del.sync(distFolder, {
      force: true,
   });
   if (!fs.existsSync(distFolder)) {
      fs.mkdirSync(distFolder);
   }
   cb?.();
}

function makeNunjucksRenderTask(sources) {
   return function () {
      return nunjucksRender(sources);
   };
}

/*
   Primary command to work with while developing.
*/
function watch(done) {
   server();

   const ignore = ['!*~', '!#*#']; // temporary editor files

   const htmlWatcher = gulpWatch(["src/html/**/*"].concat(ignore));
   const articlesWatcher = gulpWatch(["src/html/_articles/**/*"].concat(ignore));
   const sassWatcher = gulpWatch(["src/styles/**/*.scss"].concat(ignore));
   const jsWatcher = gulpWatch(["src/js/**/*.js"].concat(ignore));
   /* const imgWatcher = */ gulpWatch([
      "src/images/**/*.jpg",
      "src/images/**/*.png",
   ].concat(ignore));

   htmlWatcher.on("change", htmlHandler);
   htmlWatcher.on("add", htmlHandler);
   htmlWatcher.on("unlink", htmlHandler);

   htmlWatcher.on("unlink", (path, stats) => {
      serverReload();
   });

   articlesWatcher.on("change", articlesHandler);
   articlesWatcher.on("add", articlesHandler);
   articlesWatcher.on("unlink", articlesHandler);

   sassWatcher.on("change", sassHandler);
   sassWatcher.on("add", sassHandler);
   sassWatcher.on("unlink", sassHandler);

   jsWatcher.on("change", jsHandler);
   jsWatcher.on("add", jsHandler);
   jsWatcher.on("unlink", jsHandler);
}

export const dev = gulp.series(devBuild, watch);

export function devBuild(done) {
   return gulp.series(
      createTmpFolders,
      resetDistFolder,
      gulp.parallel(
         scripts,
         gulpSass,
         gulpCss,
         gulp.series(
            articlesJson,
            articlesDynamic,
            articles,
            makeNunjucksRenderTask([
               "src/html/**/*",
               "!src/html/_*",
               "!src/html/_*/**/*",
               "tmp/**/*.njk",
            ])
         ),
      ),
   )(done);
}

/*
   Primary command to build a production version of the site.
*/
export function build(done) {
   return gulp.series(
      createTmpFolders,
      resetDistFolder,
      gulp.parallel(
         scripts,
         gulpSass,
         gulpCss,
         gulp.series(
            articlesJson,
            articlesDynamic,
            articles,
            makeNunjucksRenderTask([
               "src/html/**/*",
               "!src/html/_*",
               "!src/html/_*/**/*",
               "tmp/**/*.njk",
            ]),
         ),
         gulpImages,
      ),
      copyStaticFiles,
      siteMap,
   )(done);
}

export default dev;
