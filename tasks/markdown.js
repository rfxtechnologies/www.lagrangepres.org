/*
   Converts markdown files in src/html folder to html files.
   Then moves the compiled html file to the /public folder.
*/
import fs from "fs";
import frontMatter from "gulp-front-matter";
import gulp from "gulp";
import wrap from "gulp-wrap";
import data from "gulp-data";
import pluck from "gulp-pluck";
import gulpIf from "gulp-if";
import flatten from "gulp-flatten";
import debug from "gulp-debug";
import htmlmin from "gulp-htmlmin";
import colors from "colors/safe";
import beautifyCode from "gulp-beautify-code";
import markdown from "gulp-markdown";

/* determine if in production or dev mode */
const devMode = process.env.NODE_ENV !== "production";

const distFolder = "./public/pastor/articles";

export function markdownChange() {
   console.log(`markdownChange task starts`);
   return (
      gulp
         .src(["src/html/**/*.md"])
         .pipe(frontMatter())
         .pipe(markdown())
         .pipe(data(function(file) {
            let obj = {};

            fs.readdirSync('src/html/_data/').forEach(file => {
               const thisData = JSON.parse(fs.readFileSync(`src/html/_data/${ file }`));
               obj = Object.assign({}, obj, thisData);
            });

            return obj;
         }))
         .pipe(
            data(function(file) {
               let obj = {};

               fs.readdirSync("src/html/_data/").forEach(file => {
                  const thisData = JSON.parse(
                     fs.readFileSync(`src/html/_data/${file}`)
                  );

                  obj = Object.assign({}, obj, thisData);
               });

               return obj;
            })
         )
         .pipe(
            wrap(
               data => {
                  return fs
                     .readFileSync(
                        `src/html/_layouts/${data.file.frontMatter.layout}`
                     )
                     .toString();
               },
               //null,
               {},
               { engine: "nunjucks" }
            )
         )
         .on("error", function(err) {
            console.log(colors.red(err.toString()));
            this.emit("end");
         })
         //if in production then minify
         .pipe(
            gulpIf(
               false && !devMode,
               htmlmin({
                  collapseWhitespace: true
               })
            )
         )
         /* If in dev mode, beautify html */
         .pipe(
            gulpIf(
               false && devMode,
               beautifyCode({
                  indent_size: 4,
                  indent_char: " ",
                  unformatted: ["code", "pre"]
               })
            )
         )
         .on("error", function(err) {
            console.log(colors.red(err.toString()));
            this.emit("end");
         })
         /* remove parent directories so we only grab files */
         .pipe(flatten())
         /* Output html files  */
         .pipe(gulp.dest(distFolder))
         .on('end', () => {
            console.log(`markdownChange task ends`);
         })
   );
}
