const gulp       = require('gulp');
const micromatch = require('micromatch');

/**
 * MultipleWatcher --- a JavaScript class for watching multiple lists
 * of files, running one or more tasks once a batch of files is
 * changed, and running something once all tasks are completed.
 */

export class MultipleWatcher {
   /**
    * const mw = new MultipleWatcher({
    *     delay: 500,
    *     watchOptions: {
    *         // passed to gulp.watch
    *     },
    *     exclude: [`<**>/*~`, `<**>/#*#`],   // for example.  ** replaced with <**> here to avoid comment shenanigans.
    *     onSuccess: (values) => { ... },
    *     onError: (error) => { ... }
    * });
    */
   constructor(options) {
      this.delay = 500;
      this.watchOptions = {};
      if (options) {
         if ('delay'        in options) { this.delay        = options.delay; }
         if ('exclude'      in options) { this.exclude      = options.exclude; }
         if ('watchOptions' in options) { this.watchOptions = options.watchOptions; }
         if ('onSuccess'    in options) { this.onSuccess    = options.onSuccess; }
         if ('onError'      in options) { this.onError      = options.onError; }
      }
      this.watching = [];
      this.events = [];
      this.debug = process.env.MULTIPLE_WATCHER_DEBUG;
   }

   /**
    * Adds watching for one or more globs to trigger the specified
    * callback.
    *
    * If you call watch() while a watcher is running, it will not
    * affect the current watcher, but will affect future runs of the
    * watcher once you call stop() then start() again.
    *
    * The callback takes the following arguments:
    * -   an array of paths for the files matching the specified globs
    *     that have changed.
    * -   an array of event objects for those files:
    *     { eventName: <eventName>, path: <path>, stat: <stat-object> }
    *
    * Watch callbacks must return a Promise.  Promise must resolve on
    * completion of a compilation/bundling/etc. task or reject when
    * that task fails.
    *
    * EXAMPLE:
    *
    *     // npm install --save gulp-stream-to-promise
    *     import gulpStreamToPromise from 'gulp-stream-to-promise';
    *
    *     mw.watch(['src/js/<**>/*.js'], (paths, events) => {
    *         // if webpackTask returns a Promise, return it directly.
    *         return webpackTask();
    *     });
    *     mw.watch(['src/styles/<**>/*.scss'], (paths, events) => {
    *         // if the task returns a gulp stream, convert it to a
    *         // promise, then return the promise.
    *         return gulpStreamToPromise(gulpSassTask());
    *     });
    *     mw.onSuccess = () => {
    *         // one would probably refresh browsers here
    *     };
    *     mw.start();
    */
   watch(globs, callback) {
      if (typeof globs === 'string') {
         globs = [globs];
      }
      this.watching.push({
         globs: globs,
         callback: callback
      });
   }

   /**
    * Start watching.
    */
   start() {
      if (this.watcher) {
         return;
      }
      let globs = this.watching.reduce((accumulator, value) => {
         return accumulator.concat(value.globs);
      }, []);
      if ('exclude' in this) {
         if (typeof this.exclude === 'string') {
            globs = globs.concat([this.exclude]);
         } else {
            globs = globs.concat(this.exclude);
         }
      }
      this.watcher = gulp.watch(globs, this.watchOptions);
      this.watcher.on('all', (eventName, path, stat) => {
         this.enqueue(eventName, path, stat);
      });
   }

   /**
    * Stop watching.
    */
   stop() {
      if (!this.watcher) {
         return;
      }
      this.watcher.close();
      delete this.watcher;
   }

   enqueue(eventName, path, stat) {
      this.events.push({
         eventName: eventName,
         path: path,
         stat: stat
      });
      if (this.timeout) {
         clearTimeout(this.timeout);
      }
      this.timeout = setTimeout(this.runQueue.bind(this), this.delay);
   }

   runQueue() {
      clearTimeout(this.timeout);
      delete this.timeout;

      // clear the queue but get all the events in the queue first.
      const events = this.events.slice();
      this.events = [];

      const paths = events.map((event) => { return event.path; });
      const pathEventMapping = {};
      events.forEach((event) => {
         pathEventMapping[event.path] = event;
      });

      // [DEBUGGING] For detection of any files that fail to match any
      // of the globs we're watching.  I'm doing this detection because I'm
      // only 99.9% confident that concatenating glob arrays for the
      // purpose of what we're doing here works properly.
      const pathTouched = {};

      const promises = [];
      this.watching.forEach((watching) => {
         let globs = watching.globs;
         if (this.exclude) {
            if (typeof this.exclude === 'string') {
               globs = globs.concat([this.exclude]);
            } else {
               globs = globs.concat(this.exclude);
            }
         }

         const matchingPaths = micromatch(paths, globs);
         if (matchingPaths.length === 0) {
            return;
         }

         // [DEBUGGING]
         console.log(`Files matching ${watching.globs.join(', ')}:`);
         matchingPaths.forEach((path) => {
            console.log(`-   ${path}`);
         });

         // [DEBUGGING]
         matchingPaths.forEach((path) => {
            pathTouched[path] = 1;
         });

         const matchingEvents = matchingPaths.map((path) => { return pathEventMapping[path]; });

         const promise = watching.callback(matchingPaths, matchingEvents);
         promises.push(promise);
      });
      if (promises.length <= 0) {
         return;
      }

      // [DEBUGGING]
      const pathsUntouched = paths.filter((path) => {
         return !pathTouched[path];
      });
      if (pathsUntouched.length > 0) {
         console.log(`UNEXPECTED: it looks like files have been edited that do not apparently match any of these globs:`);
         this.watching.forEach((watching) => {
            console.log(`-   ${watching.globs.join(', ')}`);
         });
         console.log(`even though our watcher caught them.  Here are our detected non-matching files:`);
         pathsUntouched.forEach((path) => {
            console.log(`-   ${path}`);
         });
      }

      const promise = Promise.all(promises);
      promise.then((values) => {
         if (this.onSuccess) {
            this.onSuccess(values);
         }
      }).catch((error) => {
         if (this.onError) {
            this.onError(error);
         } else {
            console.log(error);
         }
      });
   }
}

export default MultipleWatcher;
