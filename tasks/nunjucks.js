/*
   Converts nunjucks templates in src/html folder to html files.
   Then moves the compiled html file to the /public folder.
*/
import del from "del";
import path from "path";
import fs from "fs";
import gulp from "gulp";
import gulpIf from "gulp-if";
import nunjucksRender from "gulp-nunjucks-render";
import { getJsonData } from "./util/jsonData";
import htmlmin from "gulp-htmlmin";
import data from "gulp-data";
import colors from "colors/safe";
import beautifyCode from "gulp-beautify-code";
import inlinesource from "gulp-inline-source";
import posthtml from "gulp-posthtml";
import phObfuscate from "posthtml-obfuscate";
import rename from "gulp-rename";
import markdown from "nunjucks-markdown";
import marked from "marked";
import nj from "nunjucks";
import gulpnunjucks from 'gulp-nunjucks';

/* determine if in production or dev mode */
const devMode = process.env.NODE_ENV !== "production";

const distFolder = "./public";

let jsonData = null;

/*
   Filter function to keep folders starting with '_' from being
   copied to the public folder.
*/

const templates = "src/html";
const env = new nj.Environment(new nj.FileSystemLoader(templates));

marked.setOptions({
   renderer: new marked.Renderer(),
   gfm: true,
   tables: true,
   breaks: false,
   pendantic: false,
   smartLists: true,
   smartypants: false,
});

markdown.register(env, marked);

export function nunjucks(src = [], onComplete = (f) => f) {
   console.log(`Converting page(s) to html`);

   return (
      gulp
         .src(src)
         .pipe(
            data(function (file) {
               if (!jsonData) {
                  jsonData = getJsonData();
               }

               const filePath = file.relative
                  .replace(/\\/g, "/")
                  .replace("src/html", "")
                  .replace("index.njk", "")
                  .replace(".njk", ".html");

               jsonData.cacheBust = Date.now();
               jsonData.copyYear = new Date().getFullYear();

               return jsonData;
            })
         )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            if (!devMode) {
               this.emit("end");
            }
         })
         /* Convert Nunjucks templates to HTML */
         .pipe(gulpnunjucks.compile("", { env: env }))
         // .pipe(
         //    nunjucksRender({
         //       path: ["src/html/"],
         //    })
         // )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            //this.emit('end');

            if (!devMode) {
               this.emit("end");
            }
         })
         .pipe(
            inlinesource({
               compress: true,
               rootpath: path.resolve("public"),
            })
         )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            this.emit("end");
         })
         .pipe(
            posthtml([
               phObfuscate({
                  includeMailto: true,
               }),
            ])
         )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            this.emit("end");
         })
         /* If in production mode, minify html */
         .pipe(
            gulpIf(
               false && !devMode,
               htmlmin({
                  collapseWhitespace: true,
               })
            )
         )
         /* If in dev mode, beautify html */
         .pipe(
            gulpIf(
               false && devMode,
               beautifyCode({
                  indent_size: 4,
                  indent_char: " ",
                  unformatted: ["code", "pre"],
               })
            )
         )
         .on("error", function (err) {
            console.log(colors.red(err.toString()));
            //this.emit('end');

            if (!devMode) {
               this.emit("end");
            }
         })
         .pipe(
            rename(function (path) {
               path.dirname = path.dirname.replace("src\\html\\", "");
               path.dirname = path.dirname.replace("src\\html", "");
            })
         )
         /* Output html files */
         .pipe(gulp.dest(distFolder))
         /* Delete empty nunjucks-related folders in public folder */
         .on("end", function () {
            onComplete();
            /* Delete folders/files in build folder that are not needed */
            [
               "data",
               "pastor-articles",
               "components",
               "layouts",
               "macros",
               "partials",
            ].forEach((fileFolder) => {
               const folderName = `${distFolder}/_${fileFolder}`;

               if (fs.existsSync(folderName)) {
                  del(folderName, {
                     force: true,
                  });
               }
            });
         })
   );
}
