/*
   Local Development Server
*/
import BrowserSync from 'browser-sync';
import webpack from 'webpack';

import { config as webpackConfig } from './webpack';
import { isWsl2, onServiceRunningWsl2 } from './wsl2';

const browserSyncServer = BrowserSync.create();
const bundler = webpack(webpackConfig);

export function server() {
   const config = {
      serveStatic: ['./public', './src/static', {
         dir: './src/images',
         route: '/images'
      }],
      middleware: [
         //webpackDevMiddleware(bundler, { /* options */ }),
         //webpackHotMiddleware(bundler),
      ],
   };
   browserSyncServer.init(config);
   if (isWsl2()) {
      browserSyncServer.emitter.on('service:running', onServiceRunningWsl2);
   }
   return browserSyncServer;
}

export function serverReload() {
   browserSyncServer.reload();
}
