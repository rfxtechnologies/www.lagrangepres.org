/*
   SiteMap Generator
*/
import fs from 'fs';
import gulp from 'gulp';
import sitemap from 'gulp-sitemap';
import debug from 'gulp-debug';
import colors from 'colors/safe';
import filter from 'gulp-filter';

/*
   This data file constains some site-wide details needed
   for our sitemap
*/
const siteData = JSON.parse(fs.readFileSync('src/html/_data/data.json'));

/*
   File filter function. Currently removes the Page 0 test page from the sitemap
*/
const fileFilter = filter([
   '**',
   '!*public/fonts/**/*.html',
   '!*public/assets/**/*.html',
   '!*public/wereopenthissunday/*',
   `!*public/google${'[A-Za-z0-9]'.repeat(16)}.html`,
], {
   restore: false
});

export function siteMap() {
   /* Scan for html files in the public folder */
   return gulp.src(['public/**/*.html'], {
      read: false,
   })
   /* Filter files */
   .pipe(fileFilter)
   /* Generate Sitemap */
	.pipe(sitemap({
		siteUrl: siteData.domain,
	}))
	.on('error', function(err){
		console.warn(colors.red(err));
		this.emit('end');
	})
   /* Output sitemap */
	.pipe(gulp.dest('public'));
}
