/*jshint esversion: 11 */
/*jshint -W003 */

import os from 'node:os';
import childProcess from 'node:child_process';
import colors from 'colors/safe';

export function onServiceRunningWsl2(data) {
   const port = data.port;
   const ip = getWsl2Ip();
   const wsl2Url = `http://${ip}:${port}/`;
   const hostsFile = `c:\\windows\\system32\\drivers\\etc\\hosts`;
   console.log(`You're using WSL2.  Visit: ${colors.brightBlue.bold(wsl2Url)}`);
   console.log(`You may wish to edit ${colors.brightBlue.bold(hostsFile)}`);
}

export function getWsl2Ip() {
   const output = childProcess.spawnSync('ip', ['route']).stdout.toString();
   const lines = output.split(/\r?\n/);
   for (const line of lines) {
      const match = /(?:^|\s)src\s+(\d+\.\d+\.\d+\.\d+)(?:$|\s)/.exec(line);
      if (match) {
         return match[1];
      }
   }
}

export function isWsl2() {
   return os.type() === 'Linux' && /-WSL2$/.test(os.release());
}
